#pragma semicolon 1

#include <sourcemod>
#include <adminmenu>
#include <dbi>

public Plugin:myinfo =
{
    name        = "WS Steam protect",
    author      = "ExoIT",
    description = "Steam protect",
    version     = "1.0.0",
    url         = ""
};

new Handle:h_Database;      // Database handle

new String:varUniqueUserId[10] = "sv_logecho";
new String:steamprotectLog[64] = "addons/sourcemod/logs/steamprotect.log";

// Client data
new String:sClientIp[MAXPLAYERS+1][15];
new String:sClientAuthId[MAXPLAYERS+1][32];
new String:sClientConvar[MAXPLAYERS+1][32];

public OnPluginStart()
{
    decl String:error[256]; 
    h_Database = SQL_Connect("storage-local", true, error, 256); 
    if (h_Database == INVALID_HANDLE) 
    {
        LogError(error); 
        SetFailState("Не удалось установить SQL соединение"); 
        return; 
    }

    SQL_Query(h_Database, "create table if not exists steamprotect( \
        id integer primary key autoincrement, \
        ip varchar(15) not null, \
        authid varchar(64) not null unique, \
        cvar varchar(64) not null, \
        name varchar(64) not null, \
        created integer not null);");
}


public OnClientPutInServer(client)
{
    if (IsClientInGame(client) && !IsFakeClient(client)) {
        // Store data
        GetClientIP(client, sClientIp[client], sizeof(sClientIp[]));
        // Supported SM version 1.6.X or highter
        //GetClientAuthId(client, AuthId_Steam3, sClientAuthId[client], sizeof(sClientAuthId[]));
        GetClientAuthString(client, sClientAuthId[client], sizeof(sClientAuthId[]));

        // ConVar
        QueryClientConVar(client, varUniqueUserId, ConVarQueryFinished:ConVarFinishedCallBack, client);
    }
}

public OnClientDisconnect(client)
{
    strcopy(sClientIp[client], sizeof(sClientIp[]), NULL_STRING);
    strcopy(sClientAuthId[client], sizeof(sClientAuthId[]), NULL_STRING);
    strcopy(sClientConvar[client], sizeof(sClientConvar[]), NULL_STRING);
}

public ConVarFinishedCallBack(QueryCookie:cookie, client, ConVarQueryResult:result, const String:cvarName[], const String:cvarValue[])
{
    if (result == ConVarQuery_Okay) {
        if (strlen(cvarValue) == 25) {
            strcopy(sClientConvar[client], sizeof(sClientConvar[]), cvarValue);

            // Check convar
            decl String:sQuery[254];
            Format(sQuery, sizeof(sQuery), "SELECT id, ip, authid, cvar, name FROM steamprotect WHERE authid = \"%s\" LIMIT 1", sClientAuthId[client]);
            SQL_TQuery(h_Database, SQL_FindAuthIdByConvar, sQuery, client);
        }
    } else if (IsClientConnected(client)) {
        KickClient(client, "You are not authorized");
    }
}

public SQL_FindAuthIdByConvar(Handle:owner, Handle:hndl, const String:error[], any:client)
{
    if (hndl == INVALID_HANDLE) LogError(error);

    if (SQL_FetchRow(hndl)) {
        PrintToServer("Action: FoundRecord");
        // Found record
        decl id, String:ip[15], String:authid[32], String:cvar[64], String:name[64];

        id = SQL_FetchInt(hndl, 0);
        SQL_FetchString(hndl, 1, ip, sizeof(ip));
        SQL_FetchString(hndl, 2, authid, sizeof(authid));
        SQL_FetchString(hndl, 3, cvar, sizeof(cvar));
        SQL_FetchString(hndl, 4, name, sizeof(name));

        if (!StrEqual(sClientConvar[client], cvar, false)) {
            // Logging
            PrintToServer("Action: KickClient");
            LogToFile(steamprotectLog, "Client \"%N\", IP \"%s\", SteamID \"%s\", Cvar \"%s\" no matched on database Record: %i, Client \"%s\", IP \"%s\", SteamID \"%s\", Cvar \"%s\"", \
                client, sClientIp[client], sClientAuthId[client], sClientConvar[client], id, name, ip, authid, cvar);

            // Kick client
            KickClient(client, "Client error, pid: %i, error num [100]", id);
        }
    } else {
        // Create record
        PrintToServer("Action: CreateRecord");
        decl String:sQuery[512];

        Format(sQuery, sizeof(sQuery), "INSERT INTO steamprotect (ip, authid, cvar, name, created) VALUES (\"%s\", \"%s\", \"%s\", \"%N\", %i)", \
            sClientIp[client], sClientAuthId[client], sClientConvar[client], client, GetTime());
        SQL_Query(h_Database, sQuery);
    }
}