#pragma semicolon 1
#include <sourcemod>
#include <sdktools>

public Plugin:myinfo =
{
    name        = "WS Fake Ping",
    author      = "Lemah",
    description = "",
    version     = "1.0.0",
    url         = "http://wthell.org.ua"
};

new g_iPing = -1;
new String:g_szPlayerManager[20];
new g_iPlayerManager = -1;
new currentPing = 30;

public OnPluginStart()
{
    g_iPing = FindSendPropOffs("CPlayerResource", "m_iPing");
    strcopy(g_szPlayerManager, sizeof(g_szPlayerManager), "cs_player_manager");
    g_iPlayerManager = FindEntityByClassname(GetMaxClients() + 1, g_szPlayerManager);

    new String:clientIp[15];

    for (new i=1; i<MaxClients; i++) {
        if (IsClientInGame(i)) {
            GetClientIP(i, clientIp, sizeof(clientIp));

            if (StrEqual(clientIp, "192.168.0.2")) {
                CreateTimer(0.5, SetPing, any:i, TIMER_REPEAT);
                CreateTimer(15.0, UpdatePing, any:i, TIMER_REPEAT);
            }
        }
    }
}

public Action:SetPing(Handle:timer, any:client) {
    SetEntData(g_iPlayerManager, g_iPing + (client * 4), currentPing);
}

public Action:UpdatePing(Handle:timer, any:client) {
    currentPing = GetRandomInt(30, 50);
}