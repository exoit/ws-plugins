#include <sourcemod>
#include <sdktools>

public Plugin:myinfo = 
{
    name        = "WS Announce",
    author      = "ExoIT",
    description = "",
    version     = "1.0.0",
    url         = ""
};

public OnPluginStart()
{
    HookEvent("player_spawn", OnPlayerSpawn, EventHookMode_Post);
}

public Action:OnPlayerSpawn(Handle:event, const String:name[], bool:dontBroadcast)
{
    new client = GetClientOfUserId(GetEventInt(event, "userid"));

    if (!IsFakeClient(client) && IsClientInGame(client)) {
        GivePlayerItem(client, "weapon_flashbang");
    }
}