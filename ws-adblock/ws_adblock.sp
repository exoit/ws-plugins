#pragma semicolon 1
#include <sourcemod>
#include <regex>

public Plugin:myinfo =
{
    name        = "WS Ad block",
    author      = "ExoIT",
    description = "",
    version     = "1.0.0",
    url         = ""
};

new Handle:h_gRegex = INVALID_HANDLE;
new String:adblockLog[64] = "addons/sourcemod/logs/adblock.log";

public OnPluginStart()
{
    h_gRegex = CompileRegex("(https?:\\/\\/)?([\\da-z\\.-]+)\\.([a-z\\.]{2,6})([\\/\\w \\.-]*)*\\/?");
}

public OnClientPutInServer(client)
{
    if (client &&
        IsClientConnected(client) &&
        IsClientInGame(client) &&
        !IsFakeClient(client))
    {
        new String:name[128], String:ip[15];

        GetClientName(client, name, sizeof(name));
        GetClientIP(client, ip, sizeof(ip));

        // remove spaces
        TrimString(name);

        // check name
        if (MatchRegex(h_gRegex, name)) {
            LogToFile(adblockLog, "Banned client \"%N\", IP \"%s\", have url on name", client, ip);
            BanIdentity(ip, 10, BANFLAG_IP, "");
        }
    }
}