#include <sourcemod>
#include <geoip>

public Plugin:myinfo =
{
    name        = "WS Connect",
    author      = "ExoIT",
    description = "",
    version     = "3.0.0",
    url         = ""
};

public OnPluginStart()
{
    HookEvent("player_connect", capMessage, EventHookMode_Pre);
    HookEvent("player_disconnect", capMessage, EventHookMode_Pre);
}

public Action:capMessage(Handle:event, String:name[], bool:dontBroadcast) {
    return Plugin_Handled;
}

public OnClientPutInServer(client)
{
    if(client && !IsFakeClient(client) && IsClientInGame(client)) {
        new String:sBuffer[254], String:ip[15], String:geo[3];

        GetClientIP(client, ip, sizeof(ip));
        GeoipCode2(ip, geo);
        Format(sBuffer, sizeof(sBuffer), "\x04%N \x03входит \x01[\x03%s\x01]", client, geo);

        for(new i=1; i<=MaxClients; i++) {
            if(IsClientInGame(i)) {
                PrintToChat(i, sBuffer);
            }
        }
    }
}