#include <sourcemod>
#include <geoip>

public Plugin:myinfo =
{
    name        = "WS Connect announce",
    author      = "ExoIT",
    description = "",
    version     = "3.0.0",
    url         = ""
};

#define IN 1
#define OUT 2

new String:clientIp[MAXPLAYERS+1][17];
new String:clientGeoCode[MAXPLAYERS+1][3];
new Handle:welcomeMessageTimer[MAXPLAYERS+1];

public OnPluginStart()
{
    HookEvent("player_connect", capMessage, EventHookMode_Pre);
    HookEvent("player_disconnect", capMessage, EventHookMode_Pre);
}

public Action:capMessage(Handle:event, String:name[], bool:dontBroadcast) {
    return Plugin_Handled;
}

public OnClientPostAdminCheck(client) { showMessage(IN, client); }
public OnClientDisconnect(client) { showMessage(OUT, client); }

showMessage(type, client)
{
    switch (type)
    {
        case IN: {
            welcomeMessageTimer[client] = CreateTimer(7.0, welcomeMessage, client, TIMER_FLAG_NO_MAPCHANGE);
        }
        case OUT: { messageOut(client); }
    }
}

public Action:welcomeMessage(Handle:timer, any:client)
{
    if ()
    if(client && IsClientInGame(client) && !IsFakeClient(client)) {
        messageIn(client);
    }

    welcomeMessageTimer[client] = INVALID_HANDLE;
    return Plugin_Stop;
}

messageIn(client)
{
    new String:sBuffer[256];

    GetClientIP(client, clientIp[client], sizeof(clientIp));
    GeoipCode2(clientIp[client], clientGeoCode[client]);

    if (!isHidden(client))
    {
        for(new i=1; i<=MaxClients; i++)
        {
            if(IsClientInGame(i))
            {
                if(GetUserAdmin(i) != INVALID_ADMIN_ID) {
                    // Display for admins
                    Format(sBuffer, sizeof(sBuffer), "\x04%N \x03вошел \x01(\x03%s\x01) %s", client, clientIp[client], clientGeoCode[client]);
                } else {
                    // Display for players
                    if (StrEqual(clientGeoCode[client], NULL_STRING, false)) {
                        Format(sBuffer, sizeof(sBuffer), "\x04%N \x03вошел", client);
                    } else {
                        Format(sBuffer, sizeof(sBuffer), "\x04%N \x03вошел \x01[\x03%s\x01]", client, clientGeoCode[client]);
                    }
                }

                PrintToChat(i, sBuffer);
            }
        }
    }
}

messageOut(client)
{
    new String:sBuffer[256];

    if (!isHidden(client))
    {
        for(new i=1; i<=MaxClients; i++)
        {
            if(IsClientInGame(i))
            {
                if(GetUserAdmin(i) != INVALID_ADMIN_ID) {
                    // Display for admins
                    Format(sBuffer, sizeof(sBuffer), "\x04%N \x03вышел \x01(\x03%s\x01) %s", client, clientIp[client], clientGeoCode[client]);
                } else {
                    // Display for players
                    if (StrEqual(clientGeoCode[client], NULL_STRING, false)) {
                        Format(sBuffer, sizeof(sBuffer), "\x04%N \x03вышел", client);
                    } else {
                        Format(sBuffer, sizeof(sBuffer), "\x04%N \x03вышел \x01[\x03%s\x01]", client, clientGeoCode[client]);
                    }
                }

                PrintToChat(i, sBuffer);
            }
        }
    }

    strcopy(clientIp[client], sizeof(clientIp), NULL_STRING);
    strcopy(clientGeoCode[client], sizeof(clientGeoCode), NULL_STRING);
}

bool:isHidden(client)
{
    if(StrEqual(clientIp[client], "192.168.0.2") ||
        StrEqual(clientIp[client], "192.168.0.23") ||
        StrEqual(clientIp[client], "178.151.81.11"))
    {
        return true;
    } else {
        return false;
    }
}