#include <sourcemod>

public Plugin:myinfo =
{
    name        = "WS Connect announce",
    author      = "ExoIT",
    description = "",
    version     = "4.0.0",
    url         = ""
};

new bool:IsUserConnected[] = {};
new Handle:welcomeTimer[MAXPLAYERS+1] = INVALID_HANDLE;

public OnPluginStart()
{
    HookEvent("player_connect", capMsg, EventHookMode_Pre);
    HookEvent("player_disconnect", capMsg, EventHookMode_Pre);
}

public Action:capMsg(Handle:event, String:name[], bool:dontBroadcast) {
    return Plugin_Handled;
}

public OnClientPostAdminCheck(client)
{
    if (client && IsClientInGame(client) && !IsFakeClient(client)) {
        if (!IsUserConnected[GetClientUserId(client)]) {
            welcomeTimer[client] = CreateTimer(7.0, welcomeMessage, any:client, TIMER_FLAG_NO_MAPCHANGE);
        }
    }
}

public Action:welcomeMessage(Handle:timer, any:client)
{
    if (welcomeTimer[client] != INVALID_HANDLE) {
        IsUserConnected[GetClientUserId(client)] = true;
        welcomeTimer[client] = INVALID_HANDLE;

        // do display welcome message
    }
}

public OnClientDisconnect(client)
{
    new userid = GetClientUserId(client);

    if (IsUserConnected[userid]) {
        IsUserConnected[userid] = false;

        // do display message disconnect

    }
}