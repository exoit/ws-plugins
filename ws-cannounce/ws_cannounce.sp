#include <sourcemod>
#include <geoip>

public Plugin:myinfo =
{
    name        = "WS Connect announce",
    author      = "ExoIT",
    description = "",
    version     = "2.3.4",
    url         = ""
};

new bool:isAdmin[MAXPLAYERS+1] = {false, ...};
new bool:isHidden[MAXPLAYERS+1] = {false, ...};

new String:clientIp[MAXPLAYERS+1][17];
new String:clientGeoCode[MAXPLAYERS+1][3];
new String:clientCountry[MAXPLAYERS+1][46];

new Handle:welcomeMessageTimer[MAXPLAYERS+1];

public OnPluginStart()
{
    loadClients();

    HookEvent("player_connect", capMessage, EventHookMode_Pre);
    HookEvent("player_disconnect", capMessage, EventHookMode_Pre);
}

loadClients()
{
    for(new i=1; i<=MaxClients; i++)
    {
        if(IsClientInGame(i))
        {
            if(GetUserAdmin(i) != INVALID_ADMIN_ID) {
                isAdmin[i] = true;
            } else {
                isAdmin[i] = false;
            }

            GetClientIP(i, clientIp[i], sizeof(clientIp));
            GeoipCode2(clientIp[i], clientGeoCode[i]);
            GeoipCountry(clientIp[i], clientCountry[i], sizeof(clientCountry));
        }
    }
}

public Action:capMessage(Handle:event, String:name[], bool:dontBroadcast) {
    return Plugin_Handled;
}

public OnClientPostAdminCheck(client)
{
    if(GetUserAdmin(client) != INVALID_ADMIN_ID) {
        isAdmin[client] = true;
    } else {
        isAdmin[client] = false;
    }

    welcomeMessageTimer[client] = CreateTimer(10.0, welcomeMessage, client, TIMER_FLAG_NO_MAPCHANGE);

    GetClientIP(client, clientIp[client], sizeof(clientIp));
    GeoipCode2(clientIp[client], clientGeoCode[client]);
    GeoipCountry(clientIp[client], clientCountry[client], sizeof(clientCountry));

    if (StrEqual(clientIp[client], "192.168.0.23"))
    {
        strcopy(clientIp[client], sizeof(clientIp), "hidden");
        strcopy(clientGeoCode[client], sizeof(clientGeoCode), "UA");
    }

    if(StrEqual(clientIp[client], "192.168.0.2") ||
        StrEqual(clientIp[client], "178.151.81.11"))
    {
        strcopy(clientGeoCode[client], sizeof(clientGeoCode), "UA");
        isHidden[client] = true;
    }
}

public Action:welcomeMessage(Handle:timer, any:client)
{
    decl String:sBuffer[512];

    for(new i=1; i<=MaxClients; i++)
    {
        if(IsClientInGame(i))
        {
            if(isAdmin[i]) {
                // Display for admins
                if (!isHidden[i]) {
                    Format(sBuffer, sizeof(sBuffer), "\x04%N \x03вошел \x01(\x03%s\x01) %s", client, clientIp[client], clientGeoCode[client]);
                    PrintToChat(i, sBuffer);
                }
            } else {
                // Display for players
                if (StrEqual(clientGeoCode[client], NULL_STRING, false)) {
                    Format(sBuffer, sizeof(sBuffer), "\x04%N \x03вошел", client);
                } else {
                    Format(sBuffer, sizeof(sBuffer), "\x04%N \x03вошел \x01[\x03%s\x01]", client, clientGeoCode[client]);
                }

                PrintToChat(i, sBuffer);
            }
        }
    }

    welcomeMessageTimer[client] = INVALID_HANDLE;
    return Plugin_Stop;
}

public OnClientDisconnect(client)
{
    if (welcomeMessageTimer[client] != INVALID_HANDLE)
    {
        KillTimer(welcomeMessageTimer[client]);
        welcomeMessageTimer[client] = INVALID_HANDLE;
    }
    else
    {
        decl String:sBuffer[512];

        for(new i=1; i<=MaxClients; i++)
        {
            if(IsClientInGame(i))
            {
                if(isAdmin[i]) {
                    // Display for admins
                    if (!isHidden[i]) {
                        Format(sBuffer, sizeof(sBuffer), "\x04%N \x03вышел \x01(\x03%s\x01) %s", client, clientIp[client], clientGeoCode[client]);
                        PrintToChat(i, sBuffer);
                    }
                } else {
                    // Display for players
                    if (StrEqual(clientGeoCode[client], NULL_STRING, false)) {
                        Format(sBuffer, sizeof(sBuffer), "\x04%N \x03вышел", client);
                    } else {
                        Format(sBuffer, sizeof(sBuffer), "\x04%N \x03вышел \x01[\x03%s\x01]", client, clientGeoCode[client]);
                    }

                    PrintToChat(i, sBuffer);
                }
            }
        }
    }
    
    isAdmin[client] = false;
    isHidden[client] = false;

    strcopy(clientIp[client], sizeof(clientIp), NULL_STRING);
    strcopy(clientGeoCode[client], sizeof(clientGeoCode), NULL_STRING);
    strcopy(clientCountry[client], sizeof(clientCountry), NULL_STRING);
}