#pragma semicolon 1
#include <sourcemod>
#include <sdkhooks>

public Plugin:myinfo =
{
    name        = "WS Bonus",
    author      = "ExoIT",
    description = "",
    version     = "1.0.0",
    url         = ""
};

// 
new Handle:h_KvPlayers = INVALID_HANDLE;

// global vars
new bool:isMoney[MAXPLAYERS+1] = {false, ...};
new bool:isSpeed[MAXPLAYERS+1] = {false, ...};
new bool:isGravity[MAXPLAYERS+1] = {false, ...};
new bool:isAmmo[MAXPLAYERS+1] = {false, ...};
new bool:isRegeneration[MAXPLAYERS+1] = {false, ...};
new bool:isKnifelock[MAXPLAYERS+1] = {false, ...};
new bool:isDoubledamage[MAXPLAYERS+1] = {false, ...};

new Money[MAXPLAYERS+1] = {0, ...};
new Float:Speed[MAXPLAYERS+1] = {0.0, ...};
new Float:Gravity[MAXPLAYERS+1] = {1.0, ...};
new Ammo[MAXPLAYERS+1] = {0, ...};
new Regeneration[MAXPLAYERS+1] = {0, ...};
new Knifelock[MAXPLAYERS+1] = {0, ...};
new Float:Doubledamage[MAXPLAYERS+1] = {0.0, ...};

// timers
new Handle:h_HpTimer[MAXPLAYERS+1];

// offsets
new g_iAccount = -1;
new g_iSpeedOffset = -1;
//new g_iHealthOffset = -1;

public OnPluginStart()
{
    h_KvPlayers = CreateKeyValues("players");

    if (!FileToKeyValues(h_KvPlayers, "addons/sourcemod/configs/ws_bonus.cfg")) {
        SetFailState("Error load kv:ws_bonus.cfg");
    }

    // get offsets
    g_iAccount = FindSendPropOffs("CCSPlayer", "m_iAccount");
    g_iSpeedOffset = FindSendPropOffs("CCSPlayer", "m_flLaggedMovementValue");
    //g_iHealthOffset = FindSendPropOffs("CCSPlayer", "m_iHealth");

    // hooks
    HookEvent("player_spawn", OnPlayerSpawn, EventHookMode_Post);
    HookEvent("player_death", OnPlayerDeath, EventHookMode_Post);
}

public OnClientPutInServer(client)
{
    // add hook
    SDKHook(client, SDKHook_OnTakeDamage, OnTakeDamage);

    if (bool:client && !IsFakeClient(client))
    {
        new String:ip[15];
        GetClientIP(client, ip, sizeof(ip));

        PrintToServer("Bonus client: %s", ip);

        if (KvJumpToKey(h_KvPlayers, ip, false))
        {
            Money[client] = KvGetNum(h_KvPlayers, "money", 0);
            if (Money[client] != 0) { isMoney[client] = true; }

            Speed[client] = KvGetFloat(h_KvPlayers, "speed", 0.0);
            if (Speed[client] != 0.0) { isSpeed[client] = true; }

            Gravity[client] = KvGetFloat(h_KvPlayers, "gravity", 0.0);
            if (Gravity[client] != 0.0) { isGravity[client] = true; }

            Ammo[client] = KvGetNum(h_KvPlayers, "ammo", 0);
            if (Ammo[client] != 0) { isAmmo[client] = true; }

            Regeneration[client] = KvGetNum(h_KvPlayers, "regeneration", 0);
            if (Regeneration[client] != 0) { isRegeneration[client] = true; }

            Knifelock[client] = KvGetNum(h_KvPlayers, "knifelock", 0);
            if (Knifelock[client] != 0) { isKnifelock[client] = true; }

            Doubledamage[client] = KvGetFloat(h_KvPlayers, "doubledamage", 0.0);
            if (Doubledamage[client] != 0.0) { isDoubledamage[client] = true; }
        }

        KvRewind(h_KvPlayers);
    }
}

public OnClientDisconnect(client)
{
    // remove hook
    SDKUnhook(client, SDKHook_OnTakeDamage, OnTakeDamage);

    // disable
    isMoney[client] = false;
    isSpeed[client] = false;
    isGravity[client] = false;
    isAmmo[client] = false;
    isRegeneration[client] = false;
    isKnifelock[client] = false;
    isDoubledamage[client] = false;

    // def vars
    Money[client] = 0;
    Speed[client] = 0.0;
    Gravity[client] = 0.0;
    Ammo[client] = 0;
    Regeneration[client] = 0;
    Knifelock[client] = false;
    Doubledamage[client] = 0.0;

    // kill regen timer
    if (h_HpTimer[client] != INVALID_HANDLE) { KillTimer(h_HpTimer[client]); }
}

public OnPlayerSpawn(Handle:event, const String:name[], bool:dontBroadcast)
{
    new client = GetClientOfUserId(GetEventInt(event, "userid"));

    if (isMoney[client]) { SetEntData(client, g_iAccount, Money[client]); }
    if (isSpeed[client]) { SetEntDataFloat(client, g_iSpeedOffset, Speed[client]); }
    if (isGravity[client]) { SetEntityGravity(client, Gravity[client]); }
    // if (isAmmo[client]) { }
    if (isRegeneration[client]) { h_HpTimer[client] = CreateTimer(15.0, RegenHp, client, TIMER_REPEAT|TIMER_FLAG_NO_MAPCHANGE); }
    // if (isKnifelock[client]) { }
    // if (isDoubledamage[client]) { }
}

public OnPlayerDeath(Handle:event, const String:name[], bool:dontBroadcast)
{
    new client = GetClientOfUserId(GetEventInt(event, "userid"));

    if (isMoney[client]) { SetEntData(client, g_iAccount, Money[client]); }
    if (isSpeed[client]) { SetEntDataFloat(client, g_iSpeedOffset, Speed[client]); }
    if (isGravity[client]) { SetEntityGravity(client, 1.0); }
    // if (isAmmo[client]) { }
    if (isRegeneration[client]) { if (h_HpTimer[client] != INVALID_HANDLE) { KillTimer(h_HpTimer[client]); } }
    // if (isKnifelock[client]) { }
    // if (isDoubledamage[client]) { }
}

public Action:RegenHp(Handle:timer, any:client)
{
    if(bool:client && IsClientInGame(client) && IsPlayerAlive(client))
    {
        new ClientHealth = GetClientHealth(client);
        
        if (ClientHealth < 100)
        {
            if ((ClientHealth + Regeneration[client]) >= 100) {
                SetEntityHealth(client, 100);
            } else {
                SetEntityHealth(client, ClientHealth + Regeneration[client]);
            }
        }
    } else {
        return Plugin_Stop;
    }

    return Plugin_Continue;
}

public Action:OnTakeDamage(victim, &attacker, &inflictor, &Float:damage, &damagetype, &weapon, Float:damageForce[3], Float:damagePosition[3])
{
    if (isDoubledamage[attacker])
    {
        new String:sWeapon[128];
        GetClientWeapon(attacker, sWeapon, sizeof(sWeapon));
        //GetEdictClassname(inflictor, sWeapon, sizeof(sWeapon));

        if (StrEqual(sWeapon, "weapon_m4a1", false) || StrEqual(sWeapon, "weapon_ak47", false) || StrEqual(sWeapon, "weapon_deagle", false))
        {
            damage *= Doubledamage[attacker];

            // loging to console
            PrintToConsole(attacker, "########### sWeapon: %s, damage: %f, dmultiply: %f", sWeapon, damage, damage * Doubledamage[attacker]);
            return Plugin_Changed;
        }
    }
    return Plugin_Continue;
}