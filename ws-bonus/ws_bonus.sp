#pragma semicolon 1
#include <sourcemod>
#include <cstrike>

public Plugin:myinfo =
{
	name 		= "WS Bonus",
	author 		= "ExoIT",
	description = "",
	version 	= "1.0.0",
	url 		= ""
};

new Handle:h_DataBase;
new bonus[MAXPLAYERS+1];
new bonus_expires[MAXPLAYERS+1];
new Handle:h_HpTimer[MAXPLAYERS+1];

new g_iSpeedOffset = -1;

public OnPluginStart()
{
	RegServerCmd("show_vars", sVars);

	RegConsoleCmd("say", cSay);
	RegConsoleCmd("say_team", cSay);
	
	g_iSpeedOffset = FindSendPropOffs("CCSPlayer", "m_flLaggedMovementValue");
	
	HookEvent("round_start", OnRoundStart, EventHookMode_Post);
	
	new String:conErr[128];

	if(!SQL_CheckConfig("default"))
	{
		SetFailState("Not found configuration in file \"databases.cfg\".");
	}

	h_DataBase = SQL_Connect("default", true, conErr, sizeof(conErr));
	
	if(h_DataBase == INVALID_HANDLE)
	{
		SetFailState("Not connected to mysql server.");
	}
	
	
}

public OnClientPutInServer(client)
{
	if (IsFakeClient(client))
		return;
		
	new String:authid[32];
	
	GetClientAuthString(client, authid, sizeof(authid));
	
	//PrintToServer("Put %s", authid);
	
	new Handle:hQuery;
	new String:query[150];
	
	Format(query, sizeof(query), "SELECT bonus_type, UNIX_TIMESTAMP(expires) AS expires FROM sm_bonus WHERE authid = \'%s\' LIMIT 1", authid);
	
	/* if error */
	
	if((hQuery = SQL_Query(h_DataBase, query)) == INVALID_HANDLE)
		return;
	
	/* Success */
	
	new String:bonus_type[9];
	new expires;
	
	while(SQL_FetchRow(hQuery))
	{
		SQL_FetchString(hQuery, 0, bonus_type, sizeof(bonus_type));
		expires = SQL_FetchInt(hQuery, 1);
	}
	
	CloseHandle(hQuery);
	
	/* setup bonus */
	
	//PrintToServer("Current timestamp: %i", GetTime());
	//PrintToServer("Expires timestamp: %i", expires);
	
	bonus_expires[client] = expires;
	
	if(GetTime() >= expires)
		return;
	
	//PrintToServer("SQL Data: %s", bonus_type);

	// 1 - speed
	// 2 - hp
	// 3 - speed+hp
	
	if(StrEqual(bonus_type, "speed"))
		bonus[client] = 1;
		
	else if(StrEqual(bonus_type, "hp"))
	{
		bonus[client] = 2;
		h_HpTimer[client] = CreateTimer(10.0, RegenHp, client, TIMER_REPEAT|TIMER_FLAG_NO_MAPCHANGE);
	}
		
	else if(StrEqual(bonus_type, "speed+hp"))
	{
		bonus[client] = 3;
		h_HpTimer[client] = CreateTimer(10.0, RegenHp, client, TIMER_REPEAT|TIMER_FLAG_NO_MAPCHANGE);
	}

	//for(new i=0; i<sizeof(bonus); i++)
	//{
	//	PrintToServer("Cell %i, value: %i", i, bonus[i]);
	//}
}

public OnClientDisconnect(client)
{
	bonus[client] = 0;
	bonus_expires[client] = 0;
	
	if(h_HpTimer[client] != INVALID_HANDLE)
	{
		KillTimer(h_HpTimer[client]);
		h_HpTimer[client] = INVALID_HANDLE;
	}
}

public OnRoundStart(Handle:event, const String:name[], bool:dontBroadcast)
{
	for(new i=1; i<=MaxClients; i++)
	{
		if(IsClientInGame(i)) 
		{
			// 1 - speed
			// 2 - hp
			// 3 - speed+hp
			
			if(bonus[i] == 1 || bonus[i] == 3)
				SetEntDataFloat(i, g_iSpeedOffset, 1.12);
				
			//if(bonus[i] == 2)
			//	h_HpTimer[i] = CreateTimer(10.0, RegenHp, i, TIMER_REPEAT|TIMER_FLAG_NO_MAPCHANGE);
		}
	}
}


public Action:RegenHp(Handle:timer, any:client)
{
	if(bool:client && IsClientInGame(client))
	{
		new ClientHealth = GetClientHealth(client);
		
		if(ClientHealth < 100)
		{
			if(ClientHealth >= 95)
			{
				SetEntityHealth(client, 100);
			}
			else
			{
				SetEntityHealth(client, ClientHealth + 5);
			}
		}
	}
	
	return Plugin_Continue;
}

/*
	######################
	######################
*/

public Action:cSay(client, args) {

	new String:sayMsg[128];
	
	GetCmdArgString(sayMsg, sizeof(sayMsg));
	StripQuotes(sayMsg);
	TrimString(sayMsg);
	
	if(StrEqual(sayMsg, "!bonus", false))
	{
		
		if(bonus[client] != 0)
		{
			new String:type[9];
			new String:date[20];
			
			if(bonus[client] == 1)
				type = "SPEED";
			else if(bonus[client] == 2)
				type = "HP";
			else if(bonus[client] == 3)
				type = "SPEED+HP";
				
			FormatTime(date, sizeof(date), "%d.%m.%Y %H:%M", bonus_expires[client]);
		
			PrintToChat(client, "[SM] У вас бонус \"%s\", заканчивается %s.", type, date);
			return Plugin_Handled;
		}
		else
		{
			if(bonus_expires[client] != 0)
			{
				new String:date[20];
				FormatTime(date, sizeof(date), "%d.%m.%Y %H:%M", bonus_expires[client]);
				PrintToChat(client, "[SM] У вас бонус истек %s.", date);
			}
			else
			{
				PrintToChat(client, "[SM] У вас нет бонусов.");
			}
			return Plugin_Handled;
		}
	}

	return Plugin_Continue;
}

public Action:sVars(args)
{
	for(new i=1; i<MAXPLAYERS; i++)
	{
		PrintToServer("id: %i, bonus: %i, expires: %i, timer: %x", i, bonus[i], bonus_expires[i], h_HpTimer[i]);
	}
}
