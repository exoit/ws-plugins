#pragma semicolon 1
#include <sourcemod>

public Plugin:myinfo =
{
    name        = "WS Player change name locker",
    author      = "ExoIT",
    description = "",
    version     = "1.0.0",
    url         = ""
};

public OnPluginStart()
{
	HookEvent("player_changename", PlayerChangeName, EventHookMode_Pre);
}

public Action:PlayerChangeName(Handle:event, const String:name[], bool:dontBroadcast)
{
	decl client;
	client = GetClientOfUserId(GetEventInt(event, "userid"));

	if(!IsFakeClient(client))
		KickClient(client, "Извините, смена ника разрешена только вне сервера");
		
	return Plugin_Handled;
}