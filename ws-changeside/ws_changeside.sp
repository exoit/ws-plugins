#pragma semicolon 1
#include <sourcemod>
#include <sdktools>
#include <cstrike>

#define PLUGIN_VERSION "0.2f"

public Plugin:myinfo = {
    name        = "WS Change side",
    author      = "ExoIT",
    description = "Moves all players to the opposite team",
    version     = PLUGIN_VERSION,
    url         = ""
};

// Global var's
new mapTime;
new timeLeft;
new bool:changeStatus = false;
new bool:enableSilent = false;

public OnPluginStart()
{
    HookEvent("player_team", Event_PlayerTeam, EventHookMode_Pre);
    HookEvent("round_end", evRoundEnd);

    GetMapTimeLimit(mapTime);
    mapTime = mapTime*60;
    changeStatus = false;
}

public OnMapStart()
{
    GetMapTimeLimit(mapTime);
    mapTime = mapTime*60;
    changeStatus = false;
}

public Action:Event_PlayerTeam(Handle:event, const String:name[], bool:dontBroadcast)
{
    if (enableSilent)
    {
        if (!dontBroadcast) {
            SetEventBroadcast(event, true);
        }
    }

    return Plugin_Continue;
}

new TScore, CTScore;
public Action:evRoundEnd(Handle:event, const String:name[], bool:dontBroadcast)
{
    GetMapTimeLeft(timeLeft);

    if (changeStatus)
    {
        new winner = GetEventInt(event, "winner");

        if (winner == CS_TEAM_T) {
            TScore++;
        }

        if (winner == CS_TEAM_CT) {
            CTScore++;
        }

        SetTeamScore(CS_TEAM_T, TScore);
        SetTeamScore(CS_TEAM_CT, CTScore);
    }

    if (timeLeft != 0)
    {
        if ((timeLeft <= mapTime/2) && !changeStatus)
        {
            TScore  = GetTeamScore(CS_TEAM_CT);
            CTScore = GetTeamScore(CS_TEAM_T);

            enableSilent = true;

            for (new i=1; i<=GetMaxClients(); i++)
            {
                if (IsClientInGame(i) && IsClientConnected(i))
                {
                    if (GetClientTeam(i) == CS_TEAM_CT) {
                        CS_SwitchTeam(i, CS_TEAM_T);
                    }

                    else if (GetClientTeam(i) == CS_TEAM_T) {
                        CS_SwitchTeam(i, CS_TEAM_CT);
                    }
                }
            }

            SetTeamScore(CS_TEAM_T, TScore);
            SetTeamScore(CS_TEAM_CT, CTScore);

            PrintToChatAll("\x03Внимание! \x04Произошла смена команд местами.");
            changeStatus = true;
            enableSilent = false;
        }
    }
}