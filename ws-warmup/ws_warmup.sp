#pragma semicolon 1
#include <sourcemod>
#include <sdktools>
#include <cstrike>

public Plugin:myinfo =
{
    name        = "WS Warmup",
    author      = "ExoIT",
    description = "",
    version     = "1.0.0",
    url         = ""
};

new Float:warmupTime = 30.0;
new warmupIterator;

public OnPluginStart() {
    HookEvent("player_death", OnPlayerDeath, EventHookMode_Post);
}

public OnMapStart() {
    //ServerCommand("mp_startmoney 16000");
    ServerCommand("mp_friendlyfire 0");
    warmupIterator = RoundToZero(warmupTime);
    CreateTimer(1.0, AnnounceHud, _, TIMER_REPEAT|TIMER_FLAG_NO_MAPCHANGE);
    CreateTimer(warmupTime, WarmupEnd, _, TIMER_FLAG_NO_MAPCHANGE);
}

public Action:AnnounceHud(Handle:timer) {
    PrintCenterTextAll("Разогрев: %is", --warmupIterator);
    if (warmupIterator == 0) {
        return Plugin_Stop;
    }
    return Plugin_Continue; 
}

public Action:WarmupEnd(Handle:timer) {
    //ServerCommand("mp_startmoney 800");
    ServerCommand("mp_friendlyfire 1");
    ServerCommand("mp_restartgame 1");
}

public Action:OnPlayerDeath(Handle:event, const String:name[], bool:dontBroadcast)
{
    decl client;
    if (warmupIterator != 0) {
        client = GetClientOfUserId(GetEventInt(event, "userid"));
        CreateTimer(1.0, RespawnPlayer, client, TIMER_FLAG_NO_MAPCHANGE);
    }
}

public Action:RespawnPlayer(Handle:timer, any:client) {
    CS_RespawnPlayer(client);
}
