#pragma semicolon 1
#include <sourcemod>
#include <sdkhooks>
#include <regex>

public Plugin:myinfo =
{
    name        = "Alias detect",
    author      = "Lemah",
    description = "",
    version     = "1.0.0",
    url         = "http://wthell.org.ua"
};

// global var Kv
new Handle:kvAliases = INVALID_HANDLE;

// file Kv
new String:pathKvAliasesFile[64] = "addons/sourcemod/configs/blockedAliases.cfg";

// pattern for search
new Handle:h_Pattern = INVALID_HANDLE;

public OnPluginStart()
{
    kvAliases = CreateKeyValues("aliases");
    if (!FileToKeyValues(kvAliases, pathKvAliasesFile)) {
        LogError("Error load kv:blockedAliases.cfg, make new config");

        KvSetString(kvAliases, "example", "blablabla");
        KvRewind(kvAliases);

        KeyValuesToFile(kvAliases, pathKvAliasesFile);
    }
}

public OnClientPutInServer(client)
{
    decl String:clientName[64];
    GetClientName(client, clientName, sizeof(clientName));

    if (KvGotoFirstSubKey(kvAliases, false)) {
        //PrintToServer("KV: SubKey");
        decl String:alias[64], String:newName[64], String:pattern[128];

        do {
            KvGetSectionName(kvAliases, alias, sizeof(alias));
            KvGetString(kvAliases, NULL_STRING, newName, sizeof(newName));

            Format(pattern, sizeof(pattern), "%s", alias);
            h_Pattern = CompileRegex(pattern);

            if (MatchRegex(h_Pattern, clientName) > 0) {

                //PrintToServer("\x04Игрок \"%N\", переименован системой \"%s\"", client, newName);

                for (new i=1; i<MaxClients; i++) {
                    if (IsClientInGame(i)) {
                        if (GetUserAdmin(i) != INVALID_ADMIN_ID) {
                            PrintToChat(i, "\x04Игрок \"%N\", переименован системой \"%s\"", client, newName);
                        }
                    }
                }

                ClientCommand(client, "name %s", newName);
                break;
            }

        } while (KvGotoNextKey(kvAliases, false));
    }
    KvRewind(kvAliases);
}