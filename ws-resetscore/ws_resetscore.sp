#include <sourcemod>
#include <cstrike>

#define PLUGIN_VERSION "0.1d"

public Plugin:myinfo = {
	name 		= "WS Reset score",
	author 		= "ExoIT",
	description = "Reset your score",
	version		= PLUGIN_VERSION,
	url 		= ""
};

// Global var's
new String:eMsg[3][9] = {"!resetscore", "!rs", "!кы"};
new String:sayMsg[128];

public OnPluginStart() {
	RegConsoleCmd("say", cSay);
	RegConsoleCmd("say_team", cSay);
}

public Action:cSay(id, args) {
	GetCmdArgString(sayMsg, sizeof(sayMsg));
	StripQuotes(sayMsg);
	TrimString(sayMsg);

	for(new i=0; i<sizeof(eMsg); i++) {
		if(StrEqual(sayMsg, eMsg[i], false)) {
			if(IsClientInGame(id) && GetClientTeam(id) != CS_TEAM_SPECTATOR) {
				SetEntProp(id, Prop_Data, "m_iFrags", 0);
				SetEntProp(id, Prop_Data, "m_iDeaths", 0);
				PrintToChat(id, "[SM] Ваш счет был успешно сброшен.");
				return Plugin_Handled;
			} else {
				PrintToChat(id, "[SM] Вы должны быть в игре чтоб сбросить счет.");
				return Plugin_Handled;
			}
		}
	}
	return Plugin_Continue;
}