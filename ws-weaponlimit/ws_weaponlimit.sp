#pragma semicolon 1
#include <sourcemod>
#include <sdktools>
#include <sdkhooks>
#include <cstrike>

public Plugin:myinfo =
{
    name        = "WS Weapon Limit",
    author      = "ExoIT",
    description = "",
    version     = "1.0.0",
    url         = ""
};

new limit, countCT, countT;
new bool:disableLimit = false;

public OnPluginStart()
{
    HookEvent("round_start", OnRoundStart, EventHookMode_Post);

    for (new i=1; i<MaxClients; i++) {
        if (IsClientInGame(i) && !IsFakeClient(i))
            SetHook(i);
    }
}

public OnMapStart() {
    decl String:mapName[32];
    GetCurrentMap(mapName, sizeof(mapName));

    if (StrContains(mapName, "awp_", false) == 0) {
        disableLimit = true;
    } else {
        disableLimit = false;
    }
}

public OnClientPutInServer(client)
{
    if (!disableLimit) {
        SetHook(client);
    }
}

public OnClientDisconnect(client)
{
    if (!disableLimit && IsClientInGame(client)) {
        SDKUnhook(client, SDKHook_WeaponEquip, OnWeaponEquip);
        SDKUnhook(client, SDKHook_WeaponDrop, OnWeaponDrop);
    }
}

SetHook(client) {
    SDKHook(client, SDKHook_WeaponEquip, OnWeaponEquip);
    SDKHook(client, SDKHook_WeaponDrop, OnWeaponDrop);
}

public Action:OnWeaponEquip(client, weapon)
{
    decl String:sWeapon[32];
    GetEdictClassname(weapon, sWeapon, sizeof(sWeapon));

    if(!disableLimit && StrEqual(sWeapon, "weapon_awp")) {
        new team = GetClientTeam(client);
        if ((team == CS_TEAM_CT && countCT >= limit) ||
            (team == CS_TEAM_T && countT >= limit))
        {
            PrintToChat(client, "\x04Простите, AWP разрешен в размере %i на команду", limit);
            return Plugin_Handled;
        } else {
            if (team == CS_TEAM_T) {
                countT++;
            } else if (team == CS_TEAM_CT) {
                countCT++;
            }
        }

        //PrintToServer("Action: EQUIP, countT: %i, countCT: %i, limit: %i", countT, countCT, limit);
    }

    return Plugin_Continue;
}

public Action:OnWeaponDrop(client, weapon) 
{
    if (weapon != -1) {
        decl String:sWeapon[32];
        GetEdictClassname(weapon, sWeapon, sizeof(sWeapon));

        if(!disableLimit && StrEqual(sWeapon, "weapon_awp")) {
            new team = GetClientTeam(client);

            if (team == CS_TEAM_T) {
                countT--;
            } else if (team == CS_TEAM_CT) {
                countCT--;
            }

            //PrintToServer("Action: DROP, countT: %i, countCT: %i, limit: %i", countT, countCT, limit);
        }
    }

    return Plugin_Continue;
}

public Action:CS_OnBuyCommand(client, const String:weapon[])
{
    if (!disableLimit && StrEqual(weapon, "awp"))
    {
        new team = GetClientTeam(client);

        if ((team == CS_TEAM_T && countT >= limit) ||
            (team == CS_TEAM_CT && countCT >= limit))
        {
            PrintToChat(client, "\x04Простите, AWP разрешен в размере %i на команду", limit);
            return Plugin_Handled;
        }
    }
    return Plugin_Continue;
}

public Action:OnRoundStart(Handle:event, const String:name[], bool:dontBroadcast)
{
    if (!disableLimit) {
        new totalPlayers, lCountT, lCountCT;

        for (new i=1; i<MaxClients; i++) {
            if (IsClientInGame(i) && IsPlayerAlive(i) && !IsFakeClient(i)) {

                decl String:sWeapon[64];
                new weaponIndex = GetPlayerWeaponSlot(i, 0); // 0 - Primary

                if (weaponIndex != -1) {
                    GetEdictClassname(weaponIndex, sWeapon, sizeof(sWeapon));

                    if (StrEqual(sWeapon, "weapon_awp", false))
                    {
                        new team = GetClientTeam(i);
                        if (team == CS_TEAM_T) {
                            lCountT++;
                        } else if (team == CS_TEAM_CT) {
                            lCountCT++;
                        }
                    }
                }

                totalPlayers++;
            }
        }

        // set count awp
        countT = lCountT;
        countCT = lCountCT;

        if (totalPlayers > 12 && totalPlayers <= 20) {
            limit = 1;
        } else if (totalPlayers > 20 && totalPlayers <= 25) {
            limit = 2;
        } else if (totalPlayers > 25) {
            limit = 3;
        }
    }
}