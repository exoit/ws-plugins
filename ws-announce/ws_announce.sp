#include <sourcemod>

public Plugin:myinfo = 
{
    name        = "WS Announce",
    author      = "ExoIT",
    description = "",
    version     = "1.0.0",
    url         = ""
};

new Handle:h_AnnouncePanel = INVALID_HANDLE;

public OnPluginStart()
{
    h_AnnouncePanel = CreatePanel();

    new String:message[512];
    Format(message, sizeof(message), "С 1.06.17 сервер будет переведен в режим авторизации по ключу,\nпожалуйста пройдите регистрацию http://wthell.org.ua/login");
    DrawPanelText(h_AnnouncePanel, message);
    DrawPanelItem(h_AnnouncePanel, "Я прочел"); 
}

public OnClientPutInServer(client)
{
    if (client && IsClientInGame(client) && !IsFakeClient(client))
    {
        SendPanelToClient(h_AnnouncePanel, client, SelectOptionAnnouncePanel, 0);
    }
}

public SelectOptionAnnouncePanel(Handle:panel, MenuAction:action, client, option) 
{
      if (action == MenuAction_Select) { }
}