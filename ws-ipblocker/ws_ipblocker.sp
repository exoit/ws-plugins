#include <sourcemod>
#include <sdktools>
#include <regex>

public Plugin:myinfo =
{
	name 		= "WS IP blocker",
	author 		= "ExoIT",
	description = "",
	version 	= "1.0.0",
	url 		= ""
};

new Handle:h_Pattern = INVALID_HANDLE;
new warnings[MAXPLAYERS+1];

public OnPluginStart()
{
	RegConsoleCmd("say", SayCallBack);
	RegConsoleCmd("say_team", SayCallBack);

	h_Pattern = CompileRegex("([0-9]{1,3})[.|,]([0-9]{1,3})[.|,]([0-9]{1,3})[.|,]([0-9]{1,3})[:]?([0-9]{1,5})?");
}

public Action:SayCallBack(client, args)
{
	decl String:message[128];

	GetCmdArgString(message, sizeof(message));

	TrimString(message);
	ReplaceString(message, sizeof(message), " ", "");

	if(MatchRegex(h_Pattern, message) > 0)
	{
		if(warnings[client] == 3)
		{
			BanClient(client, 3600, BANFLAG_AUTHID, "Реклама", "Вы были забанены за рекламу");
			return Plugin_Handled;
		}

		decl String:say_msg[128];

		switch(warnings[client])
		{
			case 0:
				say_msg = "[SM] На сервере реклама запрещена! (первое предупреждение)";
			case 1:
				say_msg = "[SM] На сервере реклама запрещена! (второе предупреждение)";
			case 2:
				say_msg = "[SM] На сервере реклама запрещена! (последнее предупреждение)";
		}

		PrintToChat(client, say_msg);

		warnings[client]++;
		return Plugin_Handled;
	}
	return Plugin_Continue;
}

public OnClientDisconnect_Post(client)
{
	warnings[client] = 0;
}