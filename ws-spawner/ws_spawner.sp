#pragma semicolon 1
#include <sourcemod>
#include <sdktools>
#include <cstrike>

public Plugin:myinfo =
{
    name        = "WS Spawner",
    author      = "ExoIT",
    description = "",
    version     = "1.0.0",
    url         = ""
};

public OnPluginStart() {
    HookEvent("player_death", OnPlayerDeath, EventHookMode_Post);
}

public Action:OnPlayerDeath(Handle:event, const String:name[], bool:dontBroadcast)
{
    decl client;
    client = GetClientOfUserId(GetEventInt(event, "userid"));
    CreateTimer(1.0, RespawnPlayer, client, TIMER_FLAG_NO_MAPCHANGE);
}

public Action:RespawnPlayer(Handle:timer, any:client) {
    CS_RespawnPlayer(client);
}