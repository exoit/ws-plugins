#pragma semicolon 1
#include <sourcemod>
#include <sdktools>
#include <cstrike>

public Plugin:myinfo =
{
    name        = "WS Rank",
    author      = "ExoIT",
    description = "",
    version     = "1.0.0",
    url         = ""
};

new String:rankMsg[3][9] = {"rank", "me", "session"};
new String:topMsg[3][9] = {"top", "top10", "top20"};

public OnPluginStart() {
    RegConsoleCmd("say", cSay);
    RegConsoleCmd("say_team", cSay);
}

public Action:cSay(id, args) {
    decl String:sayMsg[256];
    GetCmdArgString(sayMsg, sizeof(sayMsg));
    StripQuotes(sayMsg);
    TrimString(sayMsg);

    for(new i=0; i<sizeof(rankMsg); i++) {
        if(StrEqual(sayMsg, rankMsg[i], false)) {
            if(IsClientInGame(id)) {
                PrintToChat(id, "[SM] Статистика доступна на сайте: http://wthell.org.ua/stats");
                return Plugin_Handled;
            }
        }
    }

    for(new i=0; i<sizeof(topMsg); i++) {
        if(StrEqual(sayMsg, topMsg[i], false)) {
            if(IsClientInGame(id)) {
                showVGUI(id);
                return Plugin_Handled;
            }
        }
    }

    return Plugin_Continue;
}

showVGUI(client)
{
    if (bool:client && IsClientInGame(client))
    {
        new Handle:hMessage = CreateKeyValues("data");
        decl String:sBuffer[128];

        Format(sBuffer, sizeof(sBuffer), "TOP");
        KvSetString(hMessage, "title", sBuffer);
        KvSetString(hMessage, "type", "2");

        Format(sBuffer, sizeof(sBuffer), "http://wthell.org.ua/top");
        KvSetString(hMessage, "msg", sBuffer);

        ShowVGUIPanel(client, "info", hMessage, true);
        CloseHandle(hMessage);
    }
}