#pragma semicolon 1
#include <sourcemod>
#include <cstrike>

public Plugin:myinfo = 
{
    name        = "WS Save cash",
    author      = "ExoIT",
    description = "",
    version     = "1.0.0",
    url         = ""
}

new g_iAccount = -1;
new bool:g_joinedSpec[MAXPLAYERS+1] = {false, ...};
new g_ClientMoney[MAXPLAYERS+1] = {0, ...};

public OnPluginStart()
{
    g_iAccount = FindSendPropOffs("CCSPlayer", "m_iAccount");
    HookEvent("player_team", Event_PlayerTeam);
}

public Action:Event_PlayerTeam(Handle:event, const String:name[], bool:dontBroadcast)
{
    decl client, team;

    client = GetClientOfUserId(GetEventInt(event, "userid"));
    team = GetEventInt(event, "team");

    if (team == CS_TEAM_SPECTATOR)
    {
        g_ClientMoney[client] = GetEntData(client, g_iAccount);
        g_joinedSpec[client] = true;
    }
    else if (team > 1)
    {
        if (g_joinedSpec[client])
        {
            CreateTimer(0.1, RestoreCash, client, TIMER_FLAG_NO_MAPCHANGE);
            g_joinedSpec[client] = false;
        }
    }

    return Plugin_Continue;
}

public Action:RestoreCash(Handle:timer, any:client)
{
    SetEntData(client, g_iAccount, g_ClientMoney[client], 4, true);
    PrintToChat(client, "\x04Money: \x03%i$ \x04restored.", g_ClientMoney[client]);

    return Plugin_Stop;
}