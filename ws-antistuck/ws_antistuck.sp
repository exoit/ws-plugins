#include <sourcemod>
#include <sdktools>

public Plugin:myinfo = 
{
    name        = "WS Anti-Stuck",
    author      = "ExoIT",
    description = "",
    version     = "1.0.0",
    url         = ""
};


public OnPluginStart()
{
    HookEvent("round_start", OnRoundStart);
    //HookEvent("player_spawn", OnPlayerSpawn);
}

public OnRoundStart(Handle:event, const String:name[], bool:dontBroadcast)
{
    for(new i=1; i<=MaxClients; i++)
    {
        if (IsClientInGame(i) && IsPlayerAlive(i))
            SetEntProp(i, Prop_Data, "m_CollisionGroup", 17);
    }

    CreateTimer(2.0, DisableStuck, _, TIMER_FLAG_NO_MAPCHANGE);
}

/*
public OnPlayerSpawn(Handle:event, const String:name[], bool:dontBroadcast)
{
    decl client;

    client = GetClientOfUserId(GetEventInt(event, "userid"));
    SetEntProp(client, Prop_Data, "m_CollisionGroup", 17);
}
*/

public Action:DisableStuck(Handle:timer)
{
    for(new i=1; i<=MaxClients; i++)
        if (IsClientInGame(i) && IsPlayerAlive(i) && !IsFakeClient(i))
            SetEntProp(i, Prop_Data, "m_CollisionGroup", 5);
}