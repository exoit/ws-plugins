#pragma semicolon 1
#include <sourcemod>

public Plugin:myinfo =
{
    name        = "WS Easy score",
    author      = "ExoIT",
    description = "",
    version     = "1.0.0",
    url         = ""
};

new Handle:g_hDatabase = INVALID_HANDLE;
new g_iPlayer[MAXPLAYERS+1] = {0, ...};

public OnPluginStart()
{
    HookEvent("player_death", EventDeath, EventHookMode_Post);

    // rank, top ...
    AddCommandListener(CommandSay, "say");
    AddCommandListener(CommandSay, "say_team");
}

public OnAllPluginsLoaded()
{
    if (SQL_CheckConfig("storage-local")) {
        decl String:error[256];
        g_hDatabase = SQL_Connect("storage-local", true, error, sizeof(error));

        if (g_hDatabase == INVALID_HANDLE) {
            SetFailState("Error SQL connect: %s", error);
        }

    } else {
        SetFailState("Error SQL check config \"default\"");
    }

    decl String:sQuery[512];
    Format(sQuery, sizeof(sQuery), "CREATE TABLE IF NOT EXISTS stats (\
        id INTEGER PRIMARY KEY AUTOINCREMENT,\
        authid VARCHAR(64) NOT NULL,\
        name VARCHAR(64),\
        kills INT DEFAULT 0,\
        death INT DEFAULT 0,\
        suicides INT DEFAULT 0,\
        lastplay INT DEFAULT 0);");

    SQL_FastQuery(g_hDatabase, sQuery);

    for (new i=1; i<MaxClients; i++)
    {
        if (IsClientInGame(i) && !IsFakeClient(i))
        {
            decl String:authid[64];
            GetClientAuthString(i, authid, sizeof(authid));

            Format(sQuery, sizeof(sQuery), "SELECT id, name FROM stats WHERE authid = '%s' LIMIT 1;", authid);
            SQL_TQuery(g_hDatabase, SQL_FindStat, sQuery, i);
        }
    }
}

public OnClientPutInServer(client)
{
    if (IsClientInGame(client) && !IsFakeClient(client)) {
        decl String:authid[64], String:sQuery[256];
        GetClientAuthString(client, authid, sizeof(authid));

        Format(sQuery, sizeof(sQuery), "SELECT id, name FROM stats WHERE authid = '%s' LIMIT 1;", authid);
        SQL_TQuery(g_hDatabase, SQL_FindStat, sQuery, client);
    }
}

public OnClientDisconnect_Post(client)
{
    decl String:sQuery[128];
    Format(sQuery, sizeof(sQuery), "UPDATE stats SET lastplay = %i WHERE id = %i;", GetTime(), g_iPlayer[client]);
    SQL_TQuery(g_hDatabase, SQL_UpdateRecord, sQuery);

    g_iPlayer[client] = 0;
}

public SQL_FindStat(Handle:owner, Handle:hndl, const String:error[], any:client)
{
    if (hndl == INVALID_HANDLE) LogError("FindStat error: %s", error);

    decl String:currentName[64], String:sQuery[256];
    GetClientName(client, currentName, sizeof(currentName));

    if (SQL_FetchRow(hndl))
    {
        decl id, String:name[64];

        id = SQL_FetchInt(hndl, 0);
        SQL_FetchString(hndl, 1, name, sizeof(name));
        
        if (!StrEqual(currentName, name, false)) {
            Format(sQuery, sizeof(sQuery), "UPDATE stats SET name = '%s' WHERE id = %i LIMIT 1;", currentName, id);
            SQL_FastQuery(g_hDatabase, sQuery);
        }

        g_iPlayer[client] = id;
        CloseHandle(hndl);
    }
    else
    {
        decl String:authid[64];

        GetClientAuthString(client, authid, sizeof(authid));
        Format(sQuery, sizeof(sQuery), "INSERT INTO stats (authid, name) VALUES ('%s', '%s');", authid, currentName);
        new Handle:result = SQL_Query(g_hDatabase, sQuery);
        g_iPlayer[client] = SQL_GetInsertId(result);

        CloseHandle(hndl);
        CloseHandle(result);
    }
}

public EventDeath(Handle:event, const String:name[], bool:dontBroadcast) 
{
    decl client, attacker;

    client = GetClientOfUserId(GetEventInt(event, "userid"));
    attacker = GetClientOfUserId(GetEventInt(event, "attacker"));

    if (!IsFakeClient(client) && !IsFakeClient(attacker))
    {
        decl String:sQuery1[128], String:sQuery2[128];
        if (client == attacker)
        {
            Format(sQuery1, sizeof(sQuery1), "UPDATE stats SET suicides = suicides + 1 WHERE id = %i;", g_iPlayer[client]);
            SQL_TQuery(g_hDatabase, SQL_UpdateRecord, sQuery1);
        } else {
            Format(sQuery1, sizeof(sQuery1), "UPDATE stats SET kills = kills + 1 WHERE id = %i;", g_iPlayer[attacker]);
            Format(sQuery2, sizeof(sQuery2), "UPDATE stats SET death = death + 1 WHERE id = %i;", g_iPlayer[client]);

            SQL_TQuery(g_hDatabase, SQL_UpdateRecord, sQuery1);
            SQL_TQuery(g_hDatabase, SQL_UpdateRecord, sQuery2);
        }
    }
}

public SQL_UpdateRecord(Handle:owner, Handle:hndl, const String:error[], any:client)
{
    if (hndl == INVALID_HANDLE) LogError("UpdateRecord error: %s", error);
    CloseHandle(hndl);
}

public Action:CommandSay(client, const String:command[], args)
{
    decl String:message[256];
    GetCmdArgString(message, sizeof(message));
    StripQuotes(message);
    TrimString(message);

    decl String:sQuery[256];
    if (StrEqual(message, "rank", false))
    {
        Format(sQuery, sizeof(sQuery), "SELECT kills, death, suicides FROM stats WHERE id = %i LIMIT 1;", g_iPlayer[client]);
        SQL_TQuery(g_hDatabase, SQL_GetRank, sQuery, client);
        return Plugin_Handled;
    }
    else if (StrEqual(message, "top", false))
    {
        Format(sQuery, sizeof(sQuery), "SELECT name, ROUND(CAST(kills AS float) / CAST(death AS float), 2) as kd FROM stats ORDER BY kd DESC LIMIT 15;");
        SQL_TQuery(g_hDatabase, SQL_GetTop, sQuery, client);
        return Plugin_Handled;
    }

    return Plugin_Continue;
}

public SQL_GetRank(Handle:owner, Handle:hndl, const String:error[], any:client)
{
    if (hndl == INVALID_HANDLE) LogError("GetRank error: %s", error);

    if (SQL_FetchRow(hndl))
    {
        decl kills, death, suicides;

        kills = SQL_FetchInt(hndl, 0);
        death = SQL_FetchInt(hndl, 1);
        suicides = SQL_FetchInt(hndl, 2);

        PrintToChat(client, "\x01[SM] \x03Ранг \x04%s\x01, \x03Убийств \x04%i\x01, \x03Смертей \x04%i\x01, \x03Суицидов \x04%i\x01", "-", kills, death, suicides);
    }
}

public SQL_GetTop(Handle:owner, Handle:hndl, const String:error[], any:client)
{
    if (hndl == INVALID_HANDLE) LogError("GetTop error: %s", error);

    decl String:name[64];
    decl Float:kd;
    new num = 1;

    new Handle:topList = CreatePanel();
    SetPanelTitle(topList, "### Топ игроков\n\n"); 

    while(SQL_FetchRow(hndl))
    {
        SQL_FetchString(hndl, 0, name, sizeof(name));
        kd = SQL_FetchFloat(hndl, 1);
        //kills = SQL_FetchInt(hndl, 1);
        //death = SQL_FetchInt(hndl, 2);
        //suicides = SQL_FetchInt(hndl, 3);

        decl String:sString[128];

        if (num < 10) {
            Format(sString, sizeof(sString), "%3.i - KD:%.2f / %s", num, kd, name);
        } else {
            Format(sString, sizeof(sString), "%i - KD:%.2f / %s", num, kd, name);
        }
        DrawPanelText(topList, sString);

        num++;
    }

    SendPanelToClient(topList, client, SelectOptionOnTopList, 0); 
}

public SelectOptionOnTopList(Handle:panel, MenuAction:action, client, option)
{

}