#include <sourcemod>
#include <sdktools>
#include <geoip>

public Plugin:myinfo = 
{
    name        = "WS Auth",
    author      = "ExoIT",
    description = "",
    version     = "1.0.0",
    url         = ""
};

/* handles */
new Handle:h_Database = INVALID_HANDLE;

/* const */
new String:wsauthLog[64] = "addons/sourcemod/logs/wauth.log";
new String:wsauthSqlLog[64] = "addons/sourcemod/logs/wauth-sql.log";

/* vars */
new String:varAuthKey[8] = "_token";
new bool:lockedChat[MAXPLAYERS+1] = {false, ...};

public OnPluginStart()
{
    LoadTranslations("wsauth.phrases");

    RegConsoleCmd("say", cSay);
    RegConsoleCmd("say_team", cSay);
}

public OnAllPluginsLoaded()
{
    if (SQL_CheckConfig("wsauth")) {
        SQL_TConnect(GotDatabase, "wsauth");
    } else {
        LogToFile(wsauthSqlLog, "Error SQL check config \"wsauth\"");
    }
}

public GotDatabase(Handle:owner, Handle:hndl, const String:error[], any:data)
{
    h_Database = hndl;

    if (h_Database == INVALID_HANDLE) {
        LogToFile(wsauthSqlLog, "Error SQL GotDatabase: %s", error);
    }

    SQL_FastQuery(h_Database, "SET CHARSET UTF-8");
    SQL_FastQuery(h_Database, "SET NAMES utf8");
}

public OnClientPutInServer(client)
{
    if (IsClientInGame(client) && IsClientConnected(client) && !IsFakeClient(client)) {

        new String:token[128];
        GetClientInfo(client, varAuthKey, token, sizeof(token));

        if (strlen(token) == 24) {
            // Request by token
            decl String:sQuery[254];
            Format(sQuery, sizeof(sQuery), "SELECT id, playername FROM users WHERE token = \"%s\" LIMIT 1", token);
            SQL_TQuery(h_Database, SQL_FindUserByToken, sQuery, client);
        } else {
            new String:remoteAddress[15], String:geo[3];
            GetClientIP(client, remoteAddress, sizeof(remoteAddress));
            GeoipCode2(remoteAddress, geo);

            if (!StrEqual(geo, "UA")) {
                KickClient(client, "%t", "Need credentials");
            } else {
                // disable voice && chat
                SetClientListeningFlags(client, VOICE_MUTED);
                lockedChat[client] = true;
            }
        }
    }
}

public SQL_FindUserByToken(Handle:owner, Handle:hndl, const String:error[], any:client)
{
    if (hndl == INVALID_HANDLE) {
        KickClient(client, "%t", "Database down");
        LogToFile(wsauthLog, "Error find user \"%N\" on database, invalid handle", client);
    }

    if (SQL_FetchRow(hndl)) {
        // Found record
        //PrintToServer("Action: User \"%N\" exists on database", client);

        if (IsClientInGame(client) && IsClientConnected(client)) {
            // enable chat
            lockedChat[client] = false;

            new id, String:remoteAddress[15];
            id = SQL_FetchInt(hndl, 0);
            GetClientIP(client, remoteAddress, sizeof(remoteAddress))

            // Update playername
            decl String:sQuery[254];
            Format(sQuery, sizeof(sQuery), "UPDATE users SET playername = \"%N\", last_address_on_server = \"%s\" WHERE id = %i", client, remoteAddress, id);
            SQL_Query(h_Database, sQuery);
        }
    } else {
        // Create record
        //PrintToServer("Action: User \"%N\" not exists on database", client);
        KickClient(client, "%t", "Invalid credentials");
    }
}

public Action:cSay(id, args)
{
    if (lockedChat[id]) {
        PrintToChat(id, "[WTH] Голосовое общение и чат запрещены, вы не прошли авторизацию/регистрацию");
        return Plugin_Handled;
    }
    return Plugin_Continue;
}