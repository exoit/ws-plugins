#include <sourcemod>

public Plugin:myinfo = 
{
    name        = "WS Auth",
    author      = "ExoIT",
    description = "",
    version     = "1.0.0",
    url         = ""
};

/* handles */
new Handle:h_Database = INVALID_HANDLE;

/* const */
new String:wsauthLog[64] = "addons/sourcemod/logs/wauth.log";
new String:wsauthSqlLog[64] = "addons/sourcemod/logs/wauth-sql.log";

/* vars */
new String:varAuthKey[8] = "_token";

public OnPluginStart()
{
    LoadTranslations("wsauth.phrases");
}

public OnAllPluginsLoaded()
{
    if (SQL_CheckConfig("wsauth")) {
        SQL_TConnect(GotDatabase, "wsauth");
    } else {
        LogToFile(wsauthSqlLog, "Error SQL check config \"wsauth\"");
    }
}

public GotDatabase(Handle:owner, Handle:hndl, const String:error[], any:data)
{
    h_Database = hndl;

    if (h_Database == INVALID_HANDLE) {
        LogToFile(wsauthSqlLog, "Error SQL GotDatabase: %s", error);
    }

    SQL_FastQuery(h_Database, "SET CHARSET UTF-8");
    SQL_FastQuery(h_Database, "SET NAMES utf8");
}

public OnClientPutInServer(client)
{
    if (IsClientInGame(client) && !IsFakeClient(client)) {
        new String:token[128];
        GetClientInfo(client, varAuthKey, token, sizeof(token));

        if (strlen(token) == 24) {

            // Request by token
            decl String:sQuery[254];
            Format(sQuery, sizeof(sQuery), "SELECT id, playername FROM users WHERE token = \"%s\" LIMIT 1", token);
            SQL_TQuery(h_Database, SQL_FindUserByToken, sQuery, client);
        } else {
            KickClient(client, "%t", "Need credentials");
        }
    }
}

public SQL_FindUserByToken(Handle:owner, Handle:hndl, const String:error[], any:client)
{
    if (hndl == INVALID_HANDLE) {
        KickClient(client, "%t", "Database down");
        LogToFile(wsauthLog, "Error find user \"%N\" on database, invalid handle", client);
    }

    if (SQL_FetchRow(hndl)) {
        // Found record
        //PrintToServer("Action: User \"%N\" exists on database", client);

        new id, String:playername[64];
        id = SQL_FetchInt(hndl, 0);
        SQL_FetchString(hndl, 1, playername, sizeof(playername));

        new String:nowPlayername[64];
        GetClientName(client, nowPlayername, sizeof(nowPlayername));

        // update playername record
        if(!StrEqual(playername, nowPlayername, false)) {
            // Update playername
            decl String:sQuery[254];
            Format(sQuery, sizeof(sQuery), "UPDATE users SET playername = \"%N\" WHERE id = %i LIMIT 1", client, id);
            SQL_Query(h_Database, sQuery);
        }

    } else {
        // Create record
        //PrintToServer("Action: User \"%N\" not exists on database", client);
        KickClient(client, "%t", "Invalid credentials");
    }
}