#pragma semicolon 1
#include <sourcemod>
#include <sdktools>

public Plugin:myinfo =
{
    name        = "WS Additional commands",
    author      = "ExoIT",
    description = "",
    version     = "1.0.0",
    url         = ""
};

public OnPluginStart()
{
    RegAdminCmd("sm_chatter", OnChatter, ADMFLAG_CUSTOM1, "Enable chatter");
    RegServerCmd("sm_setchatter", OnSetChatter, "Set chatter");
}

public Action:OnChatter(client, args)
{
    decl String:arg1[32];
    GetCmdArg(1, arg1, sizeof(arg1));

    new status = StringToInt(arg1);

    switch (status)
    {
        case 0:
        {
            SetClientListeningFlags(client, VOICE_NORMAL);
            PrintToChat(client, "[WTH] Отключено прослушивание всего чата.");
        }

        case 1:
        {
            SetClientListeningFlags(client, VOICE_LISTENALL);
            PrintToChat(client, "[WTH] Включено прослушивание всего чата.");
        }
    }
}

public Action:OnSetChatter(args)
{
    decl String:arg1[32];
    GetCmdArg(1, arg1, sizeof(arg1));

    new userid = StringToInt(arg1);

    if (userid) {
        new client = GetClientOfUserId(userid);

        SetClientListeningFlags(client, VOICE_LISTENALL);
        PrintToChat(client, "[WTH] Включено прослушивание всего чата.");
    }
}