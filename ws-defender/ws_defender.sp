#define DEBUG false
#pragma semicolon 1

#include <sourcemod>

public Plugin:myinfo =
{
    name        = "WS Command defender",
    author      = "ExoIT",
    description = "",
    version     = "1.0.0",
    url         = ""
};

public OnPluginStart()
{
    AddCommandListener(ProtectCommand, "status");
    AddCommandListener(ProtectCommand, "ping");
    //AddCommandListener(ProtectCommand, "kill");
}

public Action:ProtectCommand(client, const String:command[], args)
{
    return Plugin_Handled;
}