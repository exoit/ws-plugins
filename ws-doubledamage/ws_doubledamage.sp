#pragma semicolon 1
#include <sourcemod>
#include <sdkhooks>

public Plugin:myinfo = 
{
    name        = "WS Double damage",
    author      = "ExoIT",
    description = "",
    version     = "1.0.0",
    url         = ""
}

new bool:g_ClientDD[MAXPLAYERS+1] = {false, ...};

public OnPluginStart()
{
    decl String:clientIp[15];

    for (new i=1; i<MaxClients; i++) {
        if (IsClientInGame(i)) {
            GetClientIP(i, clientIp, sizeof(clientIp));

            if (StrEqual(clientIp, "192.168.0.2"))
                g_ClientDD[i] = true;

            SDKHook(i, SDKHook_OnTakeDamage, OnTakeDamage);
        }
    }
}

public OnClientPutInServer(client)
{
    decl String:clientIp[15];
    GetClientIP(client, clientIp, sizeof(clientIp));

    if (StrEqual(clientIp, "192.168.0.2"))
        g_ClientDD[client] = true;

    SDKHook(client, SDKHook_OnTakeDamage, OnTakeDamage);
}

public OnClientDisconnect_Post(client)
{
    g_ClientDD[client] = false;
    SDKUnhook(client, SDKHook_OnTakeDamage, OnTakeDamage);
}

public Action:OnTakeDamage(victim, &attacker, &inflictor, &Float:damage, &damagetype, &weapon, Float:damageForce[3], Float:damagePosition[3])
{
    decl String:sWeapon[32];
    GetClientWeapon(attacker, sWeapon, sizeof(sWeapon));

    if (g_ClientDD[attacker]) {
        if (StrEqual(sWeapon, "weapon_ak47", false) ||
            StrEqual(sWeapon, "weapon_m4a1", false) ||
            StrEqual(sWeapon, "weapon_deagle", false))
        {
            PrintToConsole(attacker, "########### sWeapon: %s, damage: %f, dmultiply: %f", sWeapon, damage, damage * 2.0);
            damage *= 2.0;
            return Plugin_Changed;
        }
    }

    // if (StrEqual(sWeapon, "hegrenade_projectile", false))
    // {
    //     // PrintToConsole(victim, "Вы ранили1 %N", attacker);
    //     // PrintToConsole(attacker, "Вы %i ранили2 %N", attacker, victim);
    //     if (g_ClientDD[attacker]) {
    //         // PrintToConsole(attacker, "Double damage %N", victim);

    //         damage *= g_fDamage;
    //         return Plugin_Changed;
    //     }
        
    // }

    return Plugin_Continue;
}
