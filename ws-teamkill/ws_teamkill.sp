#pragma semicolon 1
#include <sourcemod>
#include <sdktools>

public Plugin:myinfo = 
{
    name        = "WS TeamKill",
    author      = "ExoIT",
    description = "",
    version     = "1.0.0",
    url         = ""
}

new g_iAccount = -1;
new targetClient[MAXPLAYERS+1];

public OnPluginStart() {
    g_iAccount = FindSendPropOffs("CCSPlayer", "m_iAccount");

    HookEvent("player_hurt", OnPlayerHurt, EventHookMode_Post);
    HookEvent("player_death", OnPlayerDeath, EventHookMode_Post);
}

public OnPlayerHurt(Handle:Event, const String:name[], bool:dontBroadcast) 
{
    new kClient, aClient;

    kClient = GetClientOfUserId(GetEventInt(Event,"userid")); 
    aClient = GetClientOfUserId(GetEventInt(Event,"attacker"));

    if (kClient != aClient) {
        if (kClient && aClient && GetClientTeam(kClient) == GetClientTeam(aClient)) {
            PrintToChat(aClient, "\x04Будьте осторожны, убивать своих запрещено");
        }
    }
}

public OnPlayerDeath(Handle:Event, const String:name[], bool:dontBroadcast) 
{
    new kClient, aClient;

    kClient = GetClientOfUserId(GetEventInt(Event,"userid")); 
    aClient = GetClientOfUserId(GetEventInt(Event,"attacker"));

    if (kClient != aClient) {
        if (kClient && aClient && GetClientTeam(kClient) == GetClientTeam(aClient)) {
            targetClient[kClient] = aClient;
            ShowPunishMenu(kClient);
        }
    }
}

ShowPunishMenu(client)
{
    new Handle:h_Panel = CreatePanel();
    SetPanelTitle(h_Panel, "Что делать с подонком?\n");

    DrawPanelItem(h_Panel, "Убить, мстя моя жестока");
    DrawPanelItem(h_Panel, "Удалить все деньги");
    DrawPanelItem(h_Panel, "Сбросить счет");
    DrawPanelItem(h_Panel, "Превратить в сосульку");
    DrawPanelItem(h_Panel, "Понять и простить");
    DrawPanelItem(h_Panel, "(Выход) Ничего не делать");

    // DrawPanelItem(h_Panel, "...8");
    // DrawPanelItem(h_Panel, "...9");
    // DrawPanelItem(h_Panel, "...0"); // exit

    SendPanelToClient(h_Panel, client, DoPunishAction, 0);
    CloseHandle(h_Panel);
}

public DoPunishAction(Handle:h_Panel, MenuAction:action, client, option)
{
    if (action == MenuAction_Select)
    {
        if (IsClientConnected(targetClient[client]))
        {
            switch(option)
            {
                case 1:
                {
                    // kill
                    PrintToChatAll("[SM] Игрок %N не простил %N за TeamKill, убил нарушителя.", client, targetClient[client]);
                    ForcePlayerSuicide(targetClient[client]);
                }

                case 2:
                {
                    // remove money
                    PrintToChatAll("[SM] Игрок %N не простил %N за TeamKill, удалил все деньги нарушителя.", client, targetClient[client]);
                    SetEntData(targetClient[client], g_iAccount, 0);
                }

                case 3:
                {
                    // reset score
                    PrintToChatAll("[SM] Игрок %N не простил %N за TeamKill, сбросил счет нарушителя.", client, targetClient[client]);

                    SetEntProp(targetClient[client], Prop_Data, "m_iFrags", 0);
                    SetEntProp(targetClient[client], Prop_Data, "m_iDeaths", 0);
                }

                case 4:
                {
                    // freeze
                    PrintToChatAll("[SM] Игрок %N не простил %N за TeamKill, заморозил, подарил противнику шаровой фраг.", client, targetClient[client]);

                    SetEntityMoveType(targetClient[client], MOVETYPE_NONE);

                    // play sound
                    new Float:vec[3];
                    GetClientEyePosition(targetClient[client], vec);
                    EmitAmbientSound("physics/glass/glass_impact_bullet4.wav", vec, targetClient[client], SNDLEVEL_RAIDSIREN);
                }

                case 5:
                {
                    // sorry
                    PrintToChatAll("[SM] Игрок %N простил %N за TeamKill, понял и простил.", client, targetClient[client]);
                }

                case 6:
                {
                    // coward
                    PrintToChatAll("[SM] Игрок %N нифига не сделал с игроком %N за TeamKill, оба нубы.", client, targetClient[client]);
                }
            }
        }
        else
        {
            PrintToChat(client, "[SM] Простите, нарушитель видимо покинул сервер.");
        }

        targetClient[client] = 0;
    }
}