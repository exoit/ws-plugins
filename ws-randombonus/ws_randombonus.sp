#pragma semicolon 1
#include <sourcemod>
#include <sdkhooks>
#include <cstrike>

public Plugin:myinfo =
{
    name        = "WS Random bonus",
    author      = "ExoIT",
    description = "",
    version     = "1.0.0",
    url         = ""
};

new g_iSpeedOffset = -1, g_iHealthOffset = -1;

public OnPluginStart()
{
    g_iSpeedOffset = FindSendPropOffs("CCSPlayer", "m_flLaggedMovementValue");
    g_iHealthOffset = FindSendPropOffs("CCSPlayer", "m_iHealth");

    HookEvent("round_start", OnRoundStart, EventHookMode_Post);
    //HookEvent("round_end", OnRoundStart, EventHookMode_Post);
}

public OnRoundStart(Handle:event, const String:name[], bool:dontBroadcast)
{
    new cntT = 0, cntCT = 0;
    new countT[MAXPLAYERS+1], countCT[MAXPLAYERS+1];

    for(new i=1; i<=MaxClients; i++) {
        if(IsClientInGame(i) && IsPlayerAlive(i)) {
            if (GetClientTeam(i) == CS_TEAM_T) { countT[cntT++] = i; }
            if (GetClientTeam(i) == CS_TEAM_CT) { countCT[cntCT++] = i; }
        }
    }

    if (cntT > 13 && cntCT > 13) {
        new randT, randCT;

        randT = countT[GetRandomInt(1, cntT - 1)];
        randCT = countCT[GetRandomInt(1, cntCT - 1)];

        SetClientBonus(randT);
        SetClientBonus(randCT);
    }
}

SetClientBonus(client) {
    // speed
    SetEntDataFloat(client, g_iSpeedOffset, 1.16);

    // gravity
    //SetEntityGravity(client, 0.7);

    // hp
    SetEntData(client, g_iHealthOffset, 150);

    PrintToChat(client, "\x04Вы счастливчик в этом раунде, получите бонусы.");
    PrintToChat(client, "\x04Бегайте быстрее, +50HP.");
}