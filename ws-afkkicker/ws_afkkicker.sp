#pragma semicolon 1
#include <sourcemod>
#include <sdktools>
#include <cstrike>

public Plugin:myinfo =
{
    name        = "WS Random bonus",
    author      = "ExoIT",
    description = "",
    version     = "1.0.0",
    url         = ""
};

new maxChecks = 5 + 1;
new Handle:playersTimer[MAXPLAYERS+1];
new playersWarnings[MAXPLAYERS+1] = {0, ...};
new Float:playersLookPos[MAXPLAYERS+1][3];

public OnPluginStart()
{
    HookEvent("player_spawn", OnPlayerSpawn, EventHookMode_Post);
    HookEvent("player_death", OnPlayerDeath, EventHookMode_Post);
}

public OnPlayerSpawn(Handle:event, const String:name[], bool:dontBroadcast)
{
    decl client;
    client = GetClientOfUserId(GetEventInt(event, "userid"));

    if (client && IsPlayerAlive(client)) {
        CreateTimer(5.0, CheckPlayer, client, TIMER_FLAG_NO_MAPCHANGE|TIMER_REPEAT);
    }
}

public OnPlayerDeath(Handle:event, const String:name[], bool:dontBroadcast)
{
    decl client;
    client = GetClientOfUserId(GetEventInt(event, "userid"));

    if (playersTimer[client] != INVALID_HANDLE) {
        KillTimer(playersTimer[client], true);
        playersWarnings[client] = 0;
    }
}

public Action:CheckPlayer(Handle:timer, any:client)
{
    decl Float:wS_Pos[3];
    wS_GetLookPos(client, wS_Pos);

    if (playersLookPos[client][0] == 0.0 && 
        playersLookPos[client][1] == 0.0 &&
        playersLookPos[client][2] == 0.0) {

        playersLookPos[client][0] = wS_Pos[0];
        playersLookPos[client][1] = wS_Pos[1];
        playersLookPos[client][2] = wS_Pos[2];
        PrintToChat(client, "\x04First save position");

        return Plugin_Continue;

    } else if (playersLookPos[client][0] == wS_Pos[0] &&
        playersLookPos[client][1] == wS_Pos[1] &&
        playersLookPos[client][2] == wS_Pos[2]) {

        if (playersWarnings[client] >= maxChecks) {
            PrintToChat(client, "\x04Вы были перемещены из за бездействия");
            CS_SwitchTeam(client, CS_TEAM_SPECTATOR);
        }

        playersWarnings[client]++;
        PrintToChat(client, "\x04Please move");

        return Plugin_Continue;
    }

    return Plugin_Stop;
}

wS_GetLookPos(i, Float:wS_Pos[3]) 
{ 
    decl Float:EyePosition[3], Float:EyeAngles[3], Handle:h_trace; 
    GetClientEyePosition(i, EyePosition); 
    GetClientEyeAngles(i, EyeAngles); 
    h_trace = TR_TraceRayFilterEx(EyePosition, EyeAngles, MASK_SOLID, RayType_Infinite, wS_GetLookPos_Filter, i); 
    TR_GetEndPosition(wS_Pos, h_trace); 
    CloseHandle(h_trace); 
} 

public bool:wS_GetLookPos_Filter(ent, mask, any:i) { return i != ent; }