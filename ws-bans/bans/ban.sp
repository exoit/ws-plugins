new g_BanTarget[MAXPLAYERS+1];
new g_BanTargetUserId[MAXPLAYERS+1];
new g_BanTime[MAXPLAYERS+1];

public AdminMenu_Ban(Handle:topmenu,
                              TopMenuAction:action,
                              TopMenuObject:object_id,
                              param,
                              String:buffer[],
                              maxlength)
{
    if (action == TopMenuAction_DisplayOption) {
        Format(buffer, maxlength, "%T", "Ban player", param);
    } else if (action == TopMenuAction_SelectOption) {
        DisplayBanTargetMenu(param);
    }
}

DisplayBanTargetMenu(client)
{
    new Handle:menu = CreateMenu(MenuHandler_BanPlayerList);

    decl String:title[100];
    Format(title, sizeof(title), "%T:", "Ban player", client);
    SetMenuTitle(menu, title);
    SetMenuExitBackButton(menu, true);

    AddTargetsToMenu2(menu, client, COMMAND_FILTER_NO_BOTS|COMMAND_FILTER_CONNECTED);

    DisplayMenu(menu, client, MENU_TIME_FOREVER);
}

public MenuHandler_BanPlayerList(Handle:menu, MenuAction:action, param1, param2)
{
    if (action == MenuAction_End) {
        CloseHandle(menu);
    } else if (action == MenuAction_Cancel) {
        if (param2 == MenuCancel_ExitBack && hTopMenu != INVALID_HANDLE) {
            DisplayTopMenu(hTopMenu, param1, TopMenuPosition_LastCategory);
        }
    } else if (action == MenuAction_Select) {
        decl String:info[32], String:name[32];
        new userid, target;

        GetMenuItem(menu, param2, info, sizeof(info), _, name, sizeof(name));
        userid = StringToInt(info);

        if ((target = GetClientOfUserId(userid)) == 0) {
            PrintToChat(param1, "[SM] %t", "Player no longer available");
        } else if (!CanUserTarget(param1, target)) {
            PrintToChat(param1, "[SM] %t", "Unable to target");
        } else {
            g_BanTarget[param1] = target;
            g_BanTargetUserId[param1] = userid;
            DisplayBanTimeMenu(param1);
        }
    }
}

DisplayBanTimeMenu(client)
{
    new Handle:menu = CreateMenu(MenuHandler_BanTimeList);

    decl String:title[100];
    Format(title, sizeof(title), "%T: %N", "Ban player", client, g_BanTarget[client]);
    SetMenuTitle(menu, title);
    SetMenuExitBackButton(menu, true);

    AddMenuItem(menu, "0", "Permanent");
    AddMenuItem(menu, "5", "5 Минут");
    AddMenuItem(menu, "10", "10 Минут");
    AddMenuItem(menu, "30", "30 минут");
    AddMenuItem(menu, "60", "1 Час");
    AddMenuItem(menu, "240", "4 Часа");
    AddMenuItem(menu, "1440", "1 День");
    AddMenuItem(menu, "4320", "3 Дня");
    AddMenuItem(menu, "10080", "1 Неделя");

    DisplayMenu(menu, client, MENU_TIME_FOREVER);
}

public MenuHandler_BanTimeList(Handle:menu, MenuAction:action, param1, param2)
{
    if (action == MenuAction_End) {
        CloseHandle(menu);
    } else if (action == MenuAction_Cancel) {
        if (param2 == MenuCancel_ExitBack && hTopMenu != INVALID_HANDLE) {
            DisplayTopMenu(hTopMenu, param1, TopMenuPosition_LastCategory);
        }
    } else if (action == MenuAction_Select) {
        decl String:info[32];
        GetMenuItem(menu, param2, info, sizeof(info));
        g_BanTime[param1] = StringToInt(info);
        DisplayBanReasonMenu(param1);
    }
}

DisplayBanReasonMenu(client)
{
    new Handle:menu = CreateMenu(MenuHandler_BanReasonList);

    decl String:title[100];
    Format(title, sizeof(title), "%T: %N", "Ban reason", client, g_BanTarget[client]);
    SetMenuTitle(menu, title);
    SetMenuExitBackButton(menu, true);

    /* @TODO: we should either remove this or make it configurable */
    AddMenuItem(menu, "Аимбот", "Аимбот");
    AddMenuItem(menu, "Воллхак", "Воллхак");
    AddMenuItem(menu, "Спидхак", "Спидхак");
    AddMenuItem(menu, "Антифлеш", "Антифлеш");
    AddMenuItem(menu, "Мультихак", "Мультихак");
    AddMenuItem(menu, "Реклама", "Реклама");
    AddMenuItem(menu, "Тимкилл", "Тимкилл");
    AddMenuItem(menu, "Тимфлеш", "Тимфлеш");
    AddMenuItem(menu, "Выход за текстуры", "Выход за текстуры");
    AddMenuItem(menu, "Спам в чат\\микрофон", "Спам в чат\\микрофон");
    AddMenuItem(menu, "Запрещенный спрей", "Запрещенный спрей");
    AddMenuItem(menu, "Запрещенный ник", "Запрещенный ник");
    AddMenuItem(menu, "Кемперство", "Кемперство");
    AddMenuItem(menu, "AFK простой", "AFK простой");
    AddMenuItem(menu, "Оскорбление игроков", "Оскорбление игроков");
    AddMenuItem(menu, "Оскорбление администрациии", "Оскорбление администрациии");
    AddMenuItem(menu, "Большой пинг", "Большой пинг");
    AddMenuItem(menu, "Политика", "Политика");
    AddMenuItem(menu, "Другая", "Другая");

    DisplayMenu(menu, client, MENU_TIME_FOREVER);
}

public MenuHandler_BanReasonList(Handle:menu, MenuAction:action, param1, param2)
{
    if (action == MenuAction_End) {
        CloseHandle(menu);
    } else if (action == MenuAction_Cancel) {
        if (param2 == MenuCancel_ExitBack && hTopMenu != INVALID_HANDLE) {
            DisplayTopMenu(hTopMenu, param1, TopMenuPosition_LastCategory);
        }
    } else if (action == MenuAction_Select) {
        decl String:info[64];
        GetMenuItem(menu, param2, info, sizeof(info));
        PrepareBan(param1, g_BanTarget[param1], g_BanTime[param1], info);
    }
}

PrepareBan(client, target, time, const String:reason[])
{
    new originalTarget = GetClientOfUserId(g_BanTargetUserId[client]);

    if (originalTarget != target) {
        if (client == 0) {
            PrintToServer("[SM] %t", "Player no longer available");
        } else {
            PrintToChat(client, "[SM] %t", "Player no longer available");
        }

        return;
    }

    decl String:name[32];
    GetClientName(target, name, sizeof(name));

    decl String:adminUsername[64];
    new AdminId:aid = GetUserAdmin(client);
    GetAdminUsername(aid, adminUsername, sizeof(adminUsername));

    if (!time) {
        if (reason[0] == '\0') {
            ShowActivity(client, "%t", "Permabanned player", name);
        } else {
            ShowActivity(client, "%t", "Permabanned player reason", name, reason);
        }
    } else {
        if (reason[0] == '\0') {
            ShowActivity(client, "%t", "Banned player", name, time);
        } else {
            ShowActivity(client, "%t", "Banned player reason", name, time, reason);
        }
    }

    LogAction(client, target, "\"%L\" banned \"%L\" (minutes \"%d\") (reason \"%s\")", client, target, time, reason);

    // if (reason[0] == '\0')
    // {
    //     BanClient(target, time, BANFLAG_AUTHID, "Banned", "Banned", "sm_ban", client);
    // }
    // else
    // {
    //     BanClient(target, time, BANFLAG_AUTHID, reason, reason, "sm_ban", client);
    // }
    // BanIdentity(ipAddress, time, BANFLAG_IP, reason, "sm_addban", client);

    decl String:sQuery[512];
    Format(sQuery, sizeof(sQuery), "INSERT INTO bans (ip, authid, cvar, name, reason, created, expires, length, admin) \
        VALUES (\"%s\", \"%s\", \"%s\", \"%s\", \"%s\", %i, %i, %i, \"%s\")", \
        sClientIp[target], sClientAuthId[target], sClientConvar[target], name, reason, GetTime(), GetTime() + (time*60), time, adminUsername);

    SQL_TQuery(h_Database, SQL_BanPlayer, sQuery, target);
}

public SQL_BanPlayer(Handle:owner, Handle:hndl, const String:error[], any:client)
{
    if (hndl == INVALID_HANDLE) {
        LogError(error);
    } else {
        // kick client
        KickClient(client, "You are banned from this server");
    }
}