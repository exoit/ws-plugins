#pragma semicolon 1

#include <sourcemod>
#include <adminmenu>
#include <dbi>

public Plugin:myinfo =
{
    name        = "WS Bans",
    author      = "ExoIT",
    description = "Basebans system",
    version     = "1.0.0",
    url         = ""
};

// Global vars
// new ServerID = 1;
new String:MotdTitle[64] = "Message of the day";
new String:MotdUrl[64] = "http://wthell.org.ua/motd";

new Handle:hTopMenu = INVALID_HANDLE;
new Handle:h_Database;      // Database handle

new String:varUniqueUserId[10] = "sv_logecho";

// Client data
new String:sClientIp[MAXPLAYERS+1][15];
new String:sClientAuthId[MAXPLAYERS+1][32];
new String:sClientConvar[MAXPLAYERS+1][32];

#include "bans/ban.sp"

public OnPluginStart()
{
    LoadTranslations("wbans.phrases");
    //LoadTranslations("basebans.phrases");

    decl String:error[256]; 
    h_Database = SQL_Connect("wbans", true, error, 256); 
    if (h_Database == INVALID_HANDLE) {
        LogError(error); 
        SetFailState("Не удалось установить SQL соединение"); 
        return; 
    }

    SQL_Query(h_Database, "create table if not exists bans( \
        id integer primary key autoincrement, \
        ip varchar(15) not null, \
        authid varchar(64) not null, \
        cvar varchar(64) not null, \
        name varchar(64) not null, \
        reason varchar(64) not null, \
        created integer not null, \
        expires integer not null, \
        length integer not null, \
        attempts integer default 0, \
        admin varchar(64) not null);");

    new Handle:topmenu;
    if (LibraryExists("adminmenu") && ((topmenu = GetAdminTopMenu()) != INVALID_HANDLE)) {
        OnAdminMenuReady(topmenu);
    }
}

public OnAdminMenuReady(Handle:topmenu)
{
    /* Block us from being called twice */
    if (topmenu == hTopMenu) {
        return;
    }

    /* Save the Handle */
    hTopMenu = topmenu;

    /* Find the "Player Commands" category */
    new TopMenuObject:player_commands = FindTopMenuCategory(hTopMenu, ADMINMENU_PLAYERCOMMANDS);

    if (player_commands != INVALID_TOPMENUOBJECT) {
        AddToTopMenu(hTopMenu, "sm_ban", TopMenuObject_Item, AdminMenu_Ban, player_commands, "sm_ban", ADMFLAG_BAN);
    }
}

public OnClientPutInServer(client)
{
    if (IsClientInGame(client) && !IsFakeClient(client)) {
        // Store data
        GetClientIP(client, sClientIp[client], sizeof(sClientIp[]));
        // Supported SM version 1.6.X or highter
        //GetClientAuthId(client, AuthId_Steam3, sClientAuthId[client], sizeof(sClientAuthId[]));
        GetClientAuthString(client, sClientAuthId[client], sizeof(sClientAuthId[]));

        // Check IP & AuthID
        decl String:sQuery[254];
        Format(sQuery, sizeof(sQuery), "SELECT id,ip,reason,expires,length,attempts FROM bans \
            WHERE ip = \"%s\" AND (length = 0 OR expires > strftime('%%s', 'now')) LIMIT 1", sClientIp[client], sClientAuthId[client]);
        SQL_TQuery(h_Database, SQL_FindBanByIp, sQuery, client);

        // ConVar
        QueryClientConVar(client, varUniqueUserId, ConVarQueryFinished:ConVarFinishedCallBack, client);
    }
}

public OnClientDisconnect(client)
{
    strcopy(sClientIp[client], sizeof(sClientIp[]), NULL_STRING);
    strcopy(sClientAuthId[client], sizeof(sClientAuthId[]), NULL_STRING);
    strcopy(sClientConvar[client], sizeof(sClientConvar[]), NULL_STRING);
}

public ConVarFinishedCallBack(QueryCookie:cookie, client, ConVarQueryResult:result, const String:cvarName[], const String:cvarValue[])
{
    if (result == ConVarQuery_Okay) {
        strcopy(sClientConvar[client], sizeof(sClientConvar[]), cvarValue);

        if (strlen(cvarValue) == 25) {
            // Check convar
            decl String:sQuery[254];
            Format(sQuery, sizeof(sQuery), "SELECT id,ip,reason,expires,length,attempts FROM bans WHERE cvar = \"%s\" \
                AND (length = 0 OR expires > strftime('%%s', 'now')) LIMIT 1", sClientConvar[client]);
            SQL_TQuery(h_Database, SQL_FindBanByConvar, sQuery, client);
        }

        CreateTimer(0.01, TimerWelcome, client, TIMER_FLAG_NO_MAPCHANGE);
    }
    else if (IsClientConnected(client)) {
        KickClient(client, "You are not authorized");
    }
}

public Action:TimerWelcome(Handle:timer, any:client)
{
    if (bool:client && IsClientInGame(client)) {
        new Handle:hMessage = CreateKeyValues("data");
        decl String:sBuffer[128];

        Format(sBuffer, sizeof(sBuffer), MotdTitle);
        KvSetString(hMessage, "title", sBuffer);
        KvSetString(hMessage, "type", "2");

        Format(sBuffer, sizeof(sBuffer), MotdUrl);
        KvSetString(hMessage, "msg", sBuffer);

        if (strlen(sClientConvar[client]) < 25) {
            decl String:randomString[26];
            randomString = GenerateRandomString();

            Format(sBuffer, sizeof(sBuffer), "%s %s;joingame", varUniqueUserId, randomString);
            KvSetString(hMessage, "cmd", sBuffer);

            sClientConvar[client] = randomString;
        } else {
            KvSetString(hMessage, "cmd", "joingame");
        }

        ShowVGUIPanel(client, "info", hMessage, true);
        CloseHandle(hMessage);
    }

    return Plugin_Stop;
}

String:GenerateRandomString(len=25)
{
    decl String:pool[37] = "abcdefghijklmnopqrstuvwxyz0123456789";
    decl String:string[26] = "";

    for (new i=1; i<=len; i++) {
        FormatEx(string, sizeof(string), "%s%c", string, pool[GetRandomInt(0, 35)]);
    }

    return string;
}

public SQL_FindBanByIp(Handle:owner, Handle:hndl, const String:error[], any:client)
{
    if (hndl == INVALID_HANDLE) LogError(error);

    if (SQL_FetchRow(hndl))
    {
        decl id, expires, length, attempts;
        decl String:ip[15], String:reason[64];

        /* id,ip,reason,created,length,attempts */
        id = SQL_FetchInt(hndl, 0);
        SQL_FetchString(hndl, 1, ip, sizeof(ip));
        SQL_FetchString(hndl, 2, reason, sizeof(reason));

        expires = SQL_FetchInt(hndl, 3);
        length = SQL_FetchInt(hndl, 4);
        attempts = SQL_FetchInt(hndl, 5);

        if (!length || expires > GetTime()) {
            // increment
            attempts = attempts + 1;

            // Update attempts
            decl String:sQuery[128];
            Format(sQuery, sizeof(sQuery), "UPDATE bans SET attempts = %i WHERE id = %i", attempts, id);
            SQL_TQuery(h_Database, SQL_UpdateRecord, sQuery, client);

            decl String:kMessage[128];
            if (!length) {
                Format(kMessage, sizeof(kMessage), "%T", "Kick message", client, reason, "Never");
            } else {
                decl String:fDate[20];
                FormatTime(fDate, sizeof(fDate), "%d.%m.%Y %H:%M", expires);
                Format(kMessage, sizeof(kMessage), "%T", "Kick message", client, reason, fDate);
            }

            if (attempts > 3) {
                BanIdentity(sClientIp[client], 10, BANFLAG_IP, reason);
                BanIdentity(sClientAuthId[client], 10, BANFLAG_AUTHID, reason);
            }

            KickClient(client, kMessage);
        }

        //PrintToServer("FOUND BAN BY IP");
        //PrintToServer("Expires: %i, Now: %i, %i", expires, GetTime(), length);
    }
}

public SQL_FindBanByConvar(Handle:owner, Handle:hndl, const String:error[], any:client)
{
    if (hndl == INVALID_HANDLE) LogError(error);

    if (SQL_FetchRow(hndl))
    {
        decl id, expires, length, attempts;
        decl String:ip[15], String:reason[64];

        /* id,ip,reason,created,length,attempts */
        id = SQL_FetchInt(hndl, 0);
        SQL_FetchString(hndl, 1, ip, sizeof(ip));
        SQL_FetchString(hndl, 2, reason, sizeof(reason));

        expires = SQL_FetchInt(hndl, 3);
        length = SQL_FetchInt(hndl, 4);
        attempts = SQL_FetchInt(hndl, 5);

        if (!length || expires > GetTime()) {
            // increment
            attempts = attempts + 1;

            // Update ip and attempts
            decl String:sQuery[128];
            Format(sQuery, sizeof(sQuery), "UPDATE bans SET ip = \"%s\", attempts = %i WHERE id = %i", sClientIp[client], attempts, id);
            SQL_TQuery(h_Database, SQL_UpdateRecord, sQuery, client);

            decl String:kMessage[128];
            if (!length) {
                Format(kMessage, sizeof(kMessage), "%T", "Kick message", client, reason, "Never");
            } else {
                decl String:fDate[20];
                FormatTime(fDate, sizeof(fDate), "%d.%m.%Y %H:%M", expires);
                Format(kMessage, sizeof(kMessage), "%T", "Kick message", client, reason, fDate);
            }

            if (attempts > 3) {
                BanIdentity(sClientIp[client], 10, BANFLAG_IP, reason);
                BanIdentity(sClientAuthId[client], 10, BANFLAG_AUTHID, reason);
            }

            KickClient(client, kMessage);
        }

        //PrintToServer("FOUND BAN BY CVAR");
    }
}

public SQL_UpdateRecord(Handle:owner, Handle:hndl, const String:error[], any:client) {
    if (hndl == INVALID_HANDLE) LogError(error);
}